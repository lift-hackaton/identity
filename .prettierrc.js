/* eslint-disable node/global-require */
/* eslint-disable node/no-unpublished-require */
// All of these are defaults except singleQuote, but we specify them
// for explicitness
module.exports = {
  plugins: [require('prettier-plugin-tailwindcss')],
  quoteProps: 'as-needed',
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'all',
};
