# Token de Identidade Digital

## Índice
<!-- AUTO-GENERATED-CONTENT:START (TOC) -->
- [Índice](#%C3%8Dndice)
- [Descrição](#descri%C3%A7%C3%A3o)
- [Equipe do Lift Hackaton](#equipe-do-lift-hackaton)
- [Slides da Apresentação](#slides-da-apresenta%C3%A7%C3%A3o)
- [Video demonstração frontend protótipo](#video-demonstra%C3%A7%C3%A3o-frontend-prot%C3%B3tipo)
- [Páginas frontend para uso da ferramenta](#p%C3%A1ginas-frontend-para-uso-da-ferramenta)
  - [Frontend Emissor](#frontend-emissor)
  - [Frontend Utilização](#frontend-utiliza%C3%A7%C3%A3o)
- [Mecanismo de Verificação Adotado](#mecanismo-de-verifica%C3%A7%C3%A3o-adotado)
- [Snaps do Metamask Flask](#snaps-do-metamask-flask)
- [Página de desenvolvimento do Identity Snap](#p%C3%A1gina-de-desenvolvimento-do-identity-snap)
- [Contratos na Blockchain](#contratos-na-blockchain)
  - [Contrato do Token NTT de identidade](#contrato-do-token-ntt-de-identidade)
  - [Contrato do Issuer autorizados ao mint de NTT de identidade](#contrato-do-issuer-autorizados-ao-mint-de-ntt-de-identidade)
- [Informações adicionais](#informa%C3%A7%C3%B5es-adicionais)
  - [Formato dos dados salvos no Token](#formato-dos-dados-salvos-no-token)
  - [Identity Schema dos dados salvos no Metamask Flask](#identity-schema-dos-dados-salvos-no-metamask-flask)
  - [Como Depurar Snap dentro do metamask](#como-depurar-snap-dentro-do-metamask)
  - [Esboço de Frontend](#esbo%C3%A7o-de-frontend)
  - [Esboço de Diagramas](#esbo%C3%A7o-de-diagramas)
  - [Servidores de Api de Dados](#servidores-de-api-de-dados)
  - [Links para reuniões do grupo](#links-para-reuni%C3%B5es-do-grupo)
<!-- AUTO-GENERATED-CONTENT:END -->

## Descrição

Este projeto busca desenvolver um sistema de emissão e verificação segura de tokens equivalentes à uma identidade, utilizando blockchain e criptografia.

Atualmente, quando se deseja fazer uma inscrição em determinado serviço, geralmente precisamos preencher um formulário com nossos dados pessoais. Alguns provedores de serviços exigem menos ou mais informações a depender do nicho, mas sempre existe exposição em algum grau. E é por isso que segurança de dados e dinamicidade são assuntos que estão cada vez mais em voga nesta era da tecnologia e transformação digital.  A quantidade enorme de cadastros digitais que as pessoas têm que fazer e dados que precisam fornecer torna evidentes tanto o grande perigo de uma possível falha de segurança de uma empresa quanto a necessidade de promover um sistema de apresentação de credenciais mais dinâmico e rápido.
Através de um sistema de identidade digital descentralizada, uma pessoa tem controle total de sua credencial e pode comprovar quem ela é de maneira segura e ágil. Além de, para empresas,  limitar o risco, facilitar auditorias e verificar as credenciais instantaneamente. Ademais, sendo fundamentada em blockchain pública, independe de uma entidade para ser mantida e torna as interações mais transparentes.
Existem três agentes nesse processo. O cert-sign, que fará o registro da identidade na blockchain. O usuário, que vai receber e utilizar a identidade. E a seguradora, que vai receber as credenciais e validar os dados.
À luz da linha de raciocínio supracitada, nosso grupo desenvolveu um sistema de emissão e verificação de identidade.


## Equipe do Lift Hackaton

* Ademar Arvati Filho (Desenvolvedor) : 0xff2416aC6D95ee66fa095453531970291a3651a6
* Fabiano Rodrigo Alves Nascimento (Desenvolvedor): 0x97b6183621504b18Ccb97D0422c33a5D3601b862
* Pedro Sena Barbosa Holtz Yen (Desenvolvedor) : 0x9d0233135F7D734F3a9e2F86946ca6e3A231E586

## Slides da Apresentação

https://lift-hackaton.gitlab.io/identity/slides.html

## Video demonstração frontend protótipo

[![video demonstração](https://lift-hackaton.gitlab.io/identity/media/video.png)](https://gitlab.com/lift-hackaton/identity/-/raw/master/packages/doc/slides/media/show.mp4)

## Páginas frontend para uso da ferramenta

Foram criadas páginas para exemplificar o uso da identidade digital, tanto para a emissão de uma identidade digital pelo emissor e outra para a utilização da identidade digital criada

### Frontend Emissor

A emissão da identidade digital começa neste exemplo com o usuário requisitando uma identidade digital a um emissor / certificador no endereço:

https://lift-hackaton.gitlab.io/identity/cert-sign/user.html

A identidade solicitada é emitida, ou seja, o mint do token na blockchain é realizada nesta página de uso interno do certificador:

https://lift-hackaton.gitlab.io/identity/cert-sign/funcionario.html

### Frontend Utilização

A identidade digital para ser disponibilizada pelo usuário a uma entidade, por exemplo, uma seguradora na página:

https://lift-hackaton.gitlab.io/identity/seguradora/user.html

A identidade fornecida pode ser validada com o token na blockchain nesta página que seria de uso interno da seguradora:

https://lift-hackaton.gitlab.io/identity/seguradora/funcionario.html

## Mecanismo de Verificação Adotado

A verificação da identidade digital, ou melhor, de cada informação passível de credenciamento se dá da seguinte forma:
1. Etapa Geração da Credencial

O token NTT da identidade digital contém o endereço da carteira do _issuer_ e o resultado da seguinte função _signSubject_ - uma função wrapper da função _signMessage_ do pacote ether.js executada com o par de chaves do _issuer_:
<!-- AUTO-GENERATED-CONTENT:START (CODE:src=./packages/snap/src/metamask.js&lines=235-246) -->
<!-- The below code snippet is automatically added from ./packages/snap/src/metamask.js -->
```js
export const signSubject = async (subject) => {
  const privKey = await getPrivateKey();
  const ethWallet = new ethers.Wallet(privKey, provider);
  return await Object.entries(subject).reduce(
    async (previousValue, [key, value]) => {
      const result = await previousValue;
      result[key] = await ethWallet.signMessage(value);
      return result;
    },
    {},
  );
};
```
<!-- AUTO-GENERATED-CONTENT:END -->
2. Etapa Validação

Num segundo momento é realizada a consulta na blockchain nos contratos tanto de geração do Token de identidade digital quanto no contrato de cadastro de _issuer_ com o propósito de verificar se tanto o token quanto o _issuer_ continuam válidos para a informação que se deseja validar do usuário

3. Verificação

A verificação é realizada pela função _verifySubject_ - uma função wrapper da função _verifyMessage_ do pacote ether.js usando como argumentos o conteúdo da assinatura anteriormente salva na etapa 1, a informação disponibilizada pelo usuário que se deseja verificar e comparando com o endereço do _issuer_ do Token da seguinte forma:
<!-- AUTO-GENERATED-CONTENT:START (CODE:src=./packages/frontend/src/flask.ts&lines=640-642) -->
<!-- The below code snippet is automatically added from ./packages/frontend/src/flask.ts -->
```ts
    return (
      (await verifyMessageAddress(msg as string, sign as string)) === address
    );
```
<!-- AUTO-GENERATED-CONTENT:END -->


Desta maneira garantimos que na blockchain temos apenas a assinatura da identidade digital e somente quem o usuário final disponibilizou seus dados tem o conjunto de informações suficientes para validar a informação. Um ataque do tipo brute force seria capaz de revelar a informação por isso entendemos que uma evolução do código seria migrar para a tecnologia zero knowledge para esta etapa do processo.


## Snaps do Metamask Flask

https://www.npmjs.com/package/@arvati/identity-snap

Utilizamos o recurso de criar um plugin no metamask flask - snaps - que possibilita a inclusão de funções adicionais à carteira metamask, a saber:
* guarda de dados da identidade do usuário dentro do metamask
* disponibilização dos dados de identidade à critério do usuário para dapp´s
* recurso para a assinatura dos dados pelo emissor do NTT - preparo dos dados para realização do mint do token credencial da identidade

## Página de desenvolvimento do Identity Snap

https://lift-hackaton.gitlab.io/identity/dev/

## Contratos na Blockchain

### Contrato do Token NTT de identidade

Contrato do token soulbound - NTT - com as informações necessárias para verificação das informações do usuário por um emissor (issuer) por uma credencial / certificado disponível no blockchain

https://mumbai.polygonscan.com/address/0x55Fc029fcE6cd79d3DE4f9dA5727eE24F672066b

### Contrato do Issuer autorizados ao mint de NTT de identidade

Contrato separado para manter o registro dos Issuers, ou seja, ele vai manter a lista de todas as Wallets que têm permissão para emitir NTTs e a lista de subjects que eles estão autorizados a emitir

https://mumbai.polygonscan.com/address/0x6F5534F37311fC16105c435a7776bA53a0a1dc57


## Informações adicionais

### Formato dos dados salvos no Token

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=./packages/ntt/contracts/ERC4671.sol&lines=19-24) -->
<!-- The below code snippet is automatically added from ./packages/ntt/contracts/ERC4671.sol -->
```sol
    struct Token {
        address issuer;
        address owner;
        string data;
        bool valid;
    }
```
<!-- AUTO-GENERATED-CONTENT:END -->

https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Base64.sol

### Identity Schema dos dados salvos no Metamask Flask
 <!-- AUTO-GENERATED-CONTENT:START (CODE:src=./packages/snap/src/metamask.js&lines=13-25) -->
 <!-- The below code snippet is automatically added from ./packages/snap/src/metamask.js -->
 ```js
 IdentityType {
   version: number;
   subject: string;
   encrypted: boolean;
   data: string;
   user: address;
   issuer: address;
   issuerName: string;
   networkId: number;
   contract: address;
   signature: string;
   verify: boolean;
 }
 ```
 <!-- AUTO-GENERATED-CONTENT:END -->

### Como Depurar Snap dentro do metamask
Go to chrome://extensions
Find the MetaMask extension
Click on "Details"
Under "Inspect Views", click background.html

### Esboço de Frontend

https://www.figma.com/file/a60mKXcIuJgvWIki5bKRG5/Projeto-Lift-Learning-Identity

### Esboço de Diagramas

https://drive.google.com/file/d/1oEnZFwNKz1Tg3xa1uMgQLVQU_KAJ7RRN/view

https://app.diagrams.net/#G1oEnZFwNKz1Tg3xa1uMgQLVQU_KAJ7RRN

### Servidores de Api de Dados

https://web.deta.sh/home/arvati/default/overview

https://lift.deta.dev/api/issuer

https://lift.deta.dev/api/user

### Links para reuniões do grupo

https://meet.google.com/gpw-yuzg-xac

Equipe:
* fabianorodrigo@gmail.com
* pedrosbhy93@gmail.com
* aarvati@gmail.com
