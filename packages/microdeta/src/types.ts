export declare enum ActionTypes {
  // eslint-disable-next-line @typescript-eslint/no-shadow
  Set = 'set',
  Trim = 'trim',
  Increment = 'increment',
  Append = 'append',
  Prepend = 'prepend',
}
export declare class Action {
  readonly operation: ActionTypes;

  readonly value: any;

  constructor(action: ActionTypes, value?: any);
}

export declare type BasicType = string | number | boolean;

export declare type NullType = null;

export declare type UndefinedType = undefined;

export declare type ObjectType = {
  [key: string]:
    | ObjectType
    | ArrayType
    | BasicType
    | NullType
    | UndefinedType
    | Action;
};

export declare type ArrayType = (
  | ArrayType
  | ObjectType
  | BasicType
  | NullType
  | UndefinedType
)[];

export declare type CompositeType = ArrayType | ObjectType;

export declare type DetaType = ArrayType | ObjectType | BasicType;
