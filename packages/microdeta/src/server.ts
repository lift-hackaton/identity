import { FastifyInstance } from 'fastify';

const app: FastifyInstance = require('.');

app.listen({ port: 8080 }, (err, address) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  console.log(`Server listening at ${address}`);
});

export {};
