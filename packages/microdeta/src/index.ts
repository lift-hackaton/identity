import Fastify from 'fastify';
import cors from '@fastify/cors';

import { route as firstRoute } from './api/hello';
import { route as issuerRoute } from './api/issuer';
import { route as userRoute } from './api/user';
import { route as stateRoute } from './api/state';

const app = Fastify({
  logger: true,
});

app.register(cors, {
  origin: '*',
  methods: ['GET', 'PUT', 'POST', 'DELETE', 'PATCH'],
  allowedHeaders: '*',
});

app.register(firstRoute);
app.register(issuerRoute, { prefix: '/api/issuer' });
app.register(userRoute, { prefix: '/api/user' });
app.register(stateRoute, { prefix: '/api/state' });

app.get('/ping', async (_request, _reply) => {
  return 'pong\n';
});

module.exports = app;
