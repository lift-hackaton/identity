import { FastifyInstance, RequestGenericInterface } from 'fastify';
import { Base } from 'deta';
import { DetaType } from '../types';

export const route = async (fastify: FastifyInstance, _options: any) => {
  const getOptions = {};
  const postOptions = {};
  const deleteOptions = {};
  const putOptions = {};
  const patchOptions = {};
  const database = Base('issuer');

  fastify.get<RequestGenericInterface>(
    '/',
    getOptions,
    async (request, reply) => {
      try {
        reply.code(200).send((await database.fetch()).items);
      } catch (error) {
        reply.code(500).send(error);
      }
      return await reply;
    },
  );

  fastify.post<RequestGenericInterface>(
    '/',
    postOptions,
    async (request, reply) => {
      try {
        reply.code(200).send(
          await database.putMany(request.body as DetaType[], {
            expireIn: 300,
          }),
        );
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );

  fastify.patch<RequestGenericInterface>(
    '/',
    patchOptions,
    async (request, reply) => {
      try {
        const state = await database.fetch();
        await state.items.forEach(async (item) => {
          await database.delete(item.key as string);
        });

        reply.code(200).send(
          await database.putMany(request.body as DetaType[], {
            expireIn: 300,
          }),
        );
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );

  fastify.put<RequestGenericInterface>(
    '/',
    putOptions,
    async (request, reply) => {
      try {
        reply.code(200).send(
          await database.put(request.body as DetaType, undefined, {
            expireIn: 300,
          }),
        );
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );

  fastify.delete<RequestGenericInterface>(
    '/',
    deleteOptions,
    async (request, reply) => {
      try {
        const state = await database.fetch();
        await state.items.forEach(async (item) => {
          await database.delete(item.key as string);
        });
        reply.code(200).send({});
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );
};
