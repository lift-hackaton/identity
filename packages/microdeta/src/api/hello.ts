/* eslint-disable @typescript-eslint/consistent-type-definitions */
import { FastifyInstance, RequestGenericInterface } from 'fastify';

interface requestGeneric extends RequestGenericInterface {
  Querystring: {
    username: string;
    password: string;
  };
  Headers: {
    'h-Custom': string;
  };
}

export const route = async (fastify: FastifyInstance, _options: any) => {
  fastify.get<requestGeneric>(
    '/api/hello',
    {
      preValidation: (request, _reply, done) => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { username, password } = request.query;
        // eslint-disable-next-line no-negated-condition
        done(username !== 'admin' ? new Error('Must be admin') : undefined); // only validate `admin` account example
      },
    },
    async (request, _reply) => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const customerHeader = request.headers['h-Custom'];
      // do something with request data

      return { name: 'John Doe' };
    },
  );
};
