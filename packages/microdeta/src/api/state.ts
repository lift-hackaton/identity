import {
  FastifyInstance,
  RequestGenericInterface,
  RouteShorthandOptions,
} from 'fastify';
import { Base } from 'deta';
import { DetaType } from '../types';

// eslint-disable-next-line @typescript-eslint/consistent-type-definitions
interface requestUser extends RequestGenericInterface {
  Params: {
    user: string;
  };
}

export const route = async (fastify: FastifyInstance, _options: any) => {
  const getOptions: RouteShorthandOptions = {
    schema: {
      params: {
        type: 'object',
        properties: {
          user: {
            type: 'string',
          },
        },
      },
    },
  };
  const postOptions = {};
  const deleteOptions: RouteShorthandOptions = {
    schema: {
      params: {
        type: 'object',
        properties: {
          user: {
            type: 'string',
          },
        },
      },
    },
  };
  const putOptions = {};
  const patchOptions: RouteShorthandOptions = {
    schema: {
      params: {
        type: 'object',
        properties: {
          user: {
            type: 'string',
          },
        },
      },
    },
  };
  const database = Base('state');

  fastify.get<requestUser>('/:user', getOptions, async (request, reply) => {
    try {
      const { user } = request.params;
      // eslint-disable-next-line prettier/prettier, object-shorthand
      reply.code(200).send((await database.fetch({ "user": user })).items);
    } catch (error) {
      reply.code(500).send(error);
    }
    return reply;
  });

  fastify.post<RequestGenericInterface>(
    '/',
    postOptions,
    async (request, reply) => {
      try {
        reply
          .code(200)
          .send(await database.putMany(request.body as DetaType[]));
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );

  fastify.patch<requestUser>('/:user', patchOptions, async (request, reply) => {
    try {
      const { user } = request.params;
      // eslint-disable-next-line object-shorthand, prettier/prettier
      const state = await database.fetch({ "user": user });
      await state.items.forEach(async (item) => {
        await database.delete(item.key as string);
      });

      reply.code(200).send(await database.putMany(request.body as DetaType[]));
    } catch (error) {
      reply.code(500).send(error);
    }
    return reply;
  });

  fastify.put<RequestGenericInterface>(
    '/',
    putOptions,
    async (request, reply) => {
      try {
        reply
          .code(200)
          .send(await database.putMany(request.body as DetaType[]));
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );

  fastify.delete<requestUser>(
    '/:user',
    deleteOptions,
    async (request, reply) => {
      try {
        const { user } = request.params;
        // eslint-disable-next-line object-shorthand, prettier/prettier
        const state = await database.fetch({ "user": user });
        await state.items.forEach(async (item) => {
          await database.delete(item.key as string);
        });
        reply.code(200).send({});
      } catch (error) {
        reply.code(500).send(error);
      }
      return reply;
    },
  );
};
