/** @type import('hardhat/config').HardhatUserConfig */

import { HardhatUserConfig } from 'hardhat/types';
import '@nomicfoundation/hardhat-toolbox';
import '@nomiclabs/hardhat-ethers';
import '@typechain/hardhat';
import 'hardhat-change-network';
import '@cronos-labs/hardhat-cronoscan';
import '@nomicfoundation/hardhat-chai-matchers';
import '@nomiclabs/hardhat-etherscan';
import * as helpers from './utils/helpers';

import './tasks/registerIssuer';
import './tasks/getIssuers';
import './tasks/mintNTT';
import './tasks/grantTeamRole';
import './tasks/getCredentials';

helpers.config();

// MUMBAI API KEY
const MUMBAI_ALCHEMY_API_KEY_URL = getEV('MUMBAI_ALCHEMY_API_KEY_URL');
// ETHERSCAN API KEY
const POLYGONSCAN_API_KEY = getEV('POLYGONSCAN_API_KEY');

const config: HardhatUserConfig = {
  solidity: {
    version: '0.8.9',
    settings: {
      optimizer: {
        enabled: true,
        runs: 1,
      },
    },
  },
  paths: {
    sources: './contracts',
    tests: './test',
    cache: './cache',
    artifacts: './artifacts',
  },
  mocha: {
    timeout: 20000,
    // bail: true,
    // forbidOnly: true,
    // forbidPending: true,
  },
  // defaultNetwork: 'mumbai',
  networks: {
    hardhat: {},
    /* matic: {
      url: 'https://rpc-mainnet.maticvigil.com/',
      chainId: 137,
      gasMultiplier: 10,
      gasPrice: 35000000000,
      accounts: {
        mnemonic: process.env.MNEMONIC,
        initialIndex: 0,
        path: "m/44'/60'/0'/0",
        count: 10,
      },
    },*/
    mumbai: {
      url: MUMBAI_ALCHEMY_API_KEY_URL,
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
  },
  etherscan: {
    apiKey: POLYGONSCAN_API_KEY,
  },
};

export default config;

/**
 * Gets the environment variable value with the given name or throws an error if value is not set.
 *
 * @param name - name of the environment variable
 * @returns Value of the environment variable
 */
function getEV(name: string): string {
  const value = process.env[name];
  if (!value || value.trim() === '') {
    throw new Error(`Environment variable '${name}' is required`);
  }
  return value;
}
