// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

interface IRegistry {
    function isValid(address account) external view returns (bool);
}
