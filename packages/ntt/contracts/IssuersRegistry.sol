// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "./ERC4671.sol";
import "./IRegistry.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

// Uncomment this line to use console.log
// import "hardhat/console.sol";

/**
 * @notice A registry of IdentityNTT Issuers. An Issuer is a wallet authorized to mint Identity NTT's
 *
 * @dev Each issuer account registry has a address, a public Key (for encryption) and a list of subjects
 * which it is authorized to issue
 *
 */
contract IssuersRegistry is AccessControl, IRegistry {
    // Error thrown when a inexistent Issuer account is request
    error InexistentIssuer();
    // Error thrown when try to register an Issuer account without Subjects
    error IssuerWithoutSubjects();
    // Error thrown when try to execute some operation with suspended Issuer
    error SuspendedIssuer();

    /// Event emitted when an Issuer is registered successfully
    event Registered(address issuer, string name, bytes publicKey_, string[] subjects_);
    /// Event emitted when an Issuer is suspended successfully
    event Suspended(address issuer, uint256 dateTimeSuspension);
    /// Event emitted when the an Issuer's list of Subjects changes
    event SubjectsChanged(address issuer, string[] subjects_);

    // Issuer data
    struct Issuer {
        address issuer;
        string name;
        bytes publicKey;
        string[] subjects;
        uint256 dateTimeSuspension;
    }

    // Issuers's addresses array
    address[] private _issuers;
    // Mapping from issuer address to its data
    mapping(address => Issuer) private _issuersRegistry;

    // Create a new role identifier for the manager role
    bytes32 public constant MANAGER_ROLE = keccak256("MANAGER");
    // Create a new role identifier for the team role
    bytes32 public constant TEAM_ROLE = keccak256("TEAM");

    constructor(address manager_) {
        // Grant the admin role to the publisher account
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        // Grant the manager role to the informed manager account
        _setupRole(MANAGER_ROLE, manager_);
        // Sets MANAGER_ROLE as TEAM_ROLE's admin role.
        _setRoleAdmin(TEAM_ROLE, MANAGER_ROLE);
    }

    /**
     * Reverts if {issuer_} is not registered or is already suspended
     */
    modifier validateIssuer(address issuer_) {
        if (_issuersRegistry[issuer_].issuer == address(0)) {
            revert InexistentIssuer();
        }
        if (_issuersRegistry[issuer_].dateTimeSuspension != 0) {
            revert SuspendedIssuer();
        }
        _;
    }

    /**
     * @notice Register a new Issuer
     *
     * @param issuer_ The account address to be regitered as a Issuer
     * @param name_ Issuer's name
     * @param publicKey_ public key used to encrypt data if need to send some data to the issuer or verify its signature
     * @param subjects_ List of subjects the Issuer is authorized to issue
     */
    function newIssuer(
        address issuer_,
        string calldata name_,
        bytes calldata publicKey_,
        string[] calldata subjects_
    ) external virtual onlyRole(TEAM_ROLE) {
        if (subjects_.length == 0) {
            revert IssuerWithoutSubjects();
        }
        if (_issuersRegistry[issuer_].issuer == address(0)) {
            _issuers.push(issuer_);
            _issuersRegistry[issuer_] = Issuer({
                issuer: issuer_,
                name: name_,
                publicKey: publicKey_,
                subjects: subjects_,
                dateTimeSuspension: 0
            });
            emit Registered(issuer_, name_, publicKey_, subjects_);
        }
    }

    /**
     * @notice Suspends a registered Issuer
     *
     * @param issuer_  The issuer's account address to be suspended
     * @param dateTimeSuspension_  Date/time (seconds from 1970) from which the Issuer Registry
     * was suspended and all credentials after will be invalid
     */
    function suspendIssuer(address issuer_, uint56 dateTimeSuspension_)
        external
        virtual
        onlyRole(TEAM_ROLE)
        validateIssuer(issuer_)
    {
        require(
            dateTimeSuspension_ <= block.timestamp,
            "Date/Time suspension can't be future"
        );
        _issuersRegistry[issuer_].dateTimeSuspension = dateTimeSuspension_;
        emit Suspended(issuer_, dateTimeSuspension_);
    }

    /**
     * @notice Adds a new Subject to a registered Issuer
     *
     * @dev analisar se faz sentido em termos de custo fazer um loop
     * pelo array de subjects a fim de garantir que não se insira um
     * subject já existente
     *
     * @param issuer_  The issuer's account address to be added the new Subject
     * @param subject_  Subject identifier
     */
    function addIssuerSubject(address issuer_, string calldata subject_)
        external
        virtual
        onlyRole(TEAM_ROLE)
        validateIssuer(issuer_)
    {
        _issuersRegistry[issuer_].subjects.push(subject_);
        emit SubjectsChanged(issuer_, _issuersRegistry[issuer_].subjects);
    }

    /**
     * Return the Issuer Registry data
     *
     * @param issuer_  Issuer's address
     * @return Issuer data
     */
    function getIssuer(address issuer_) public view returns (Issuer memory) {
        if (_issuersRegistry[issuer_].issuer == address(0)) {
            revert InexistentIssuer();
        }
        return _issuersRegistry[issuer_];
    }

    /**
     * Return all Issuers already registered
     *
     * @return List of Issuers data
     */
    function getIssuers() external view returns (Issuer[] memory) {
        Issuer[] memory result = new Issuer[](_issuers.length);
        for (uint256 i = 0; i < _issuers.length; i++) {
            result[i] = _issuersRegistry[_issuers[i]];
        }
        return result;
    }

    /**
     * Return TRUE if the address is a registered Issuer and has a datetimeSuspension equals zero
     *
     * @param issuer_  Issuer's address
     * @return TRUE if {issuer_} is registered and dateTimeSuspension equals zero, FALSE otherwise
     */
    function isValid(address issuer_) public view returns (bool) {
        return
            _issuersRegistry[issuer_].issuer != address(0) &&
            _issuersRegistry[issuer_].dateTimeSuspension == 0;
    }
}
