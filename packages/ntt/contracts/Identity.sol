// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "./ERC4671.sol";
import "./IRegistry.sol";
import "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

// Uncomment this line to use console.log
// import "hardhat/console.sol";

/**
 * @notice Identity NTT - Non-Tradable Tokens Standard, aka badges or souldbound NFTs (EIP-4671)
 * A non-tradable token, or NTT, represents inherently personal possessions (material or immaterial),
 * such as university diplomas, online training certificates, government issued documents (national id,
 * driving license, visa, wedding, etc.), labels, and so on.
 *
 * @dev A NTT contract is seen as representing one type of certificate delivered by one authority.
 * For instance, one NTT contract for the French National Id, another for Ethereum EIP creators, and so on…
 *
 */
contract IdentityNTT is ERC4671 {
    // Create a new role identifier for the manager role
    bytes32 public constant MANAGER_ROLE = keccak256("MANAGER");
    // Create a new role identifier for the team role
    bytes32 public constant TEAM_ROLE = keccak256("TEAM");
    IRegistry private _issuerRegistry;

    modifier onlyValidIssuer() {
        require(
            _issuerRegistry.isValid(msg.sender),
            "Unauthorized. Only registered issuers can execute this operation"
        );
        _;
    }

    constructor(
        IRegistry issuerRegistryAddress_,
        string memory name_,
        string memory symbol_
    ) ERC4671(name_, symbol_) {
        //TODO: invocar supportsInterface pra validar que endereço é um IRegistry
        _issuerRegistry = issuerRegistryAddress_;
        // // Grant the admin role to the publisher account
        // _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        // // Grant the manager role to the informed manager account
        // _setupRole(MANAGER_ROLE, manager_);
        // // Sets MANAGER_ROLE as TEAM_ROLE's admin role.
        // _setRoleAdmin(TEAM_ROLE, MANAGER_ROLE);
    }

    /**
     * @notice Mint a new NTT credential
     * @param holder Address for whom to assign the token
     * @param data JSON with credential data in the format {atribute: "issuerSignedData",...}
     * @return tokenId Identifier of the minted token
     */
    function mint(address holder, string calldata data)
        external
        virtual
        onlyValidIssuer
        returns (TokenID tokenId)
    {
        //TODO: Se os dados das credenciais forem passados via JSON,
        // inviabiliza fazer uma validação no smart contract contra os subjects autorizados para o Issuer
        return _mint(holder, data);
    }

    /// @notice Mark the credential token as revoked
    /// @param tokenId Identifier of the token
    function revoke(TokenID tokenId) external virtual onlyValidIssuer {
        Token storage token = _getTokenOrRevert(tokenId);
        require(
            token.issuer == msg.sender,
            "Unauthorized. Only the token's Issuer is able to revoke it"
        );
        _revoke(token);
        emit Revoked(token.owner, tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(ERC4671)
        returns (bool)
    {
        return
            interfaceId == type(ERC4671).interfaceId ||
            interfaceId == type(IRegistry).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    /**
     * Gets the credential metadata
     *
     * @param tokenId credential ID which metadata will be returned
     */
    function getCredential(TokenID tokenId)
        external
        view
        returns (Token memory)
    {
        return _getTokenOrRevert(tokenId);
    }

    /**
     * Return a list of owner's NTTs crendetial metadata
     *
     * @param owner  Address of owner/holder of NTTs
     * @return array of all owner's NTTs metadata
     */
    function getCredentials(address owner)
        external
        view
        returns (Token[] memory)
    {
        TokenID[] memory ownersTokensId = _indexedTokenIds[owner];
        Token[] memory result = new Token[](ownersTokensId.length);
        for (uint256 i = 0; i < ownersTokensId.length; i++) {
            result[i] = _tokens[ownersTokensId[i]];
        }
        return result;
    }
}
