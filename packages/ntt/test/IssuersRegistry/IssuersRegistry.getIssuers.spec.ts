import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import {
  deployIssuerRegistryFixture,
  IIssuerRegistryFixture,
} from '../shared/fixtureIssuerRegistry';
import { getPublicKeyFromAddress } from '../shared/hardhatNetwork';

export async function issuersRegistryGetIssuers(): Promise<void> {
  context('#getIssuers', async () => {
    let fixture: IIssuerRegistryFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployIssuerRegistryFixture);
    });

    it('Should get all registered issuers', async function () {
      // o fixture já registrou a conta issuerX
      const issuerXAddress = await fixture.issuerX.getAddress();
      const issuerXPublicKey = getPublicKeyFromAddress(issuerXAddress);
      const issuerYAddress = await fixture.issuerY.getAddress();
      const issuerYName = 'Issuer Y Test';
      const issuerYPublicKey = getPublicKeyFromAddress(issuerYAddress);

      await fixture.issuersRegistry
        .connect(fixture.teamMemberA)
        .newIssuer(issuerYAddress, issuerYName, issuerYPublicKey, ['tst']);

      const issuersData = await fixture.issuersRegistry.getIssuers();
      expect(issuersData).to.be.an('array').length(2);
      // issuerX
      expect(issuersData[0].issuer).to.be.equal(issuerXAddress);
      expect(issuersData[0].publicKey).to.be.equal(issuerXPublicKey);
      expect(issuersData[0].subjects)
        .to.be.an('array')
        .length(fixture.issuerXSubjects.length);

      expect(issuersData[0].subjects[0]).to.be.equal(
        fixture.issuerXSubjects[0],
      );

      expect(issuersData[0].subjects[1]).to.be.equal(
        fixture.issuerXSubjects[1],
      );
      expect(issuersData[0].dateTimeSuspension).to.be.equal(0);
      // issuerY
      expect(issuersData[1].issuer).to.be.equal(issuerYAddress);
      expect(issuersData[1].publicKey).to.be.equal(issuerYPublicKey);
      expect(issuersData[1].subjects).to.be.an('array').length(1);
      expect(issuersData[1].subjects[0]).to.be.equal('tst');
    });
  });
}
