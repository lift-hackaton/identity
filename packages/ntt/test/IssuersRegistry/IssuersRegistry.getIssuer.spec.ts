import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import {
  deployIssuerRegistryFixture,
  IIssuerRegistryFixture,
} from "../shared/fixtureIssuerRegistry";
import { getPublicKeyFromAddress } from "../shared/hardhatNetwork";

export async function issuersRegistryGetIssuer(): Promise<void> {
  context("#getIssuer", async () => {
    let fixture: IIssuerRegistryFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployIssuerRegistryFixture);
    });

    it("Should get a registered issuer", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const issuerPublicKey = getPublicKeyFromAddress(issuerAddress);

      const issuerData = await fixture.issuersRegistry.getIssuer(issuerAddress);
      expect(issuerData.issuer).to.be.equal(issuerAddress);
      expect(issuerData.publicKey).to.be.equal(issuerPublicKey);
      expect(issuerData.subjects)
        .to.be.an("array")
        .length(fixture.issuerXSubjects.length);
      expect(issuerData.subjects[0]).to.be.equal(fixture.issuerXSubjects[0]);
      expect(issuerData.subjects[1]).to.be.equal(fixture.issuerXSubjects[1]);
      expect(issuerData.dateTimeSuspension).to.be.equal(0);
    });

    it("Should revert if try to get a not registered Issuer", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.accountA.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .getIssuer(issuerAddress)
      ).to.revertedWithCustomError(fixture.issuersRegistry, "InexistentIssuer");
    });
  });
}
