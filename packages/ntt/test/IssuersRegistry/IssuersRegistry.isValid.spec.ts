import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import {
  deployIssuerRegistryFixture,
  IIssuerRegistryFixture,
} from "../shared/fixtureIssuerRegistry";

export async function issuersRegistryIsValid(): Promise<void> {
  context("#isValid", async () => {
    let fixture: IIssuerRegistryFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployIssuerRegistryFixture);
    });

    it("Should return TRUE if address is registered and is not suspended", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();

      expect(await fixture.issuersRegistry.isValid(issuerAddress)).to.be.true;
    });
    it("Should return FALSE if address is not registered", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerY.getAddress();

      expect(await fixture.issuersRegistry.isValid(issuerAddress)).to.be.false;
    });
    it("Should return FALSE if address is registered and suspended", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);
      await fixture.issuersRegistry
        .connect(fixture.teamMemberA)
        .suspendIssuer(issuerAddress, dateTimeSuspension);

      expect(await fixture.issuersRegistry.isValid(issuerAddress)).to.be.false;
    });
  });
}
