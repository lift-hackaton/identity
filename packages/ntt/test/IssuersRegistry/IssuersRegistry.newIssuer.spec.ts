import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import {
  deployIssuerRegistryFixture,
  IIssuerRegistryFixture,
} from '../shared/fixtureIssuerRegistry';
import { getPublicKeyFromAddress } from '../shared/hardhatNetwork';

const SUBJECTS = ['cpf', 'name', 'birthDate'];

export async function issuersRegistryNew(): Promise<void> {
  context('#newIssuer', async () => {
    let fixture: IIssuerRegistryFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployIssuerRegistryFixture);
    });

    it('Should register a new issuer that was not registered yet', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerY.getAddress();
      const issuerName = 'Issuer Test';
      const issuerPublicKey = getPublicKeyFromAddress(issuerAddress);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .newIssuer(issuerAddress, issuerName, issuerPublicKey, SUBJECTS),
      )
        .to.emit(fixture.issuersRegistry, 'Registered')
        .withArgs(issuerAddress, issuerPublicKey, SUBJECTS);

      const issuerData = await fixture.issuersRegistry.getIssuer(issuerAddress);
      expect(issuerData.issuer).to.be.equal(issuerAddress);
      expect(issuerData.name).to.be.equal(issuerName);
      expect(issuerData.publicKey).to.be.equal(issuerPublicKey);
      expect(issuerData.subjects).to.be.an('array').length(3);
      expect(issuerData.subjects[0]).to.be.equal('cpf');
      expect(issuerData.subjects[1]).to.be.equal('name');
      expect(issuerData.subjects[2]).to.be.equal('birthDate');
      expect(issuerData.dateTimeSuspension).to.be.equal(0);
    });

    it('Should NOT register nor update an issuer already registered', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const issuerName = 'Issuer Test';
      const issuerPublicKey = getPublicKeyFromAddress(issuerAddress);
      const anotherAccountPublicKey = getPublicKeyFromAddress(
        await fixture.accountA.getAddress(),
      );

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .newIssuer(
            issuerAddress,
            issuerName,
            anotherAccountPublicKey,
            SUBJECTS,
          ),
      ).not.to.emit(fixture.issuersRegistry, 'Registered');

      const issuerData = await fixture.issuersRegistry.getIssuer(issuerAddress);
      expect(issuerData.issuer).to.be.equal(issuerAddress);
      expect(issuerData.name).to.be.equal('Unit Tests Issuer Registry S/A');
      expect(issuerData.publicKey).to.be.equal(issuerPublicKey);
      expect(issuerData.subjects)
        .to.be.an('array')
        .length(fixture.issuerXSubjects.length);
      expect(issuerData.subjects[0]).to.be.equal(fixture.issuerXSubjects[0]);
      expect(issuerData.subjects[1]).to.be.equal(fixture.issuerXSubjects[1]);
      expect(issuerData.dateTimeSuspension).to.be.equal(0);
    });

    it('Should revert if register an issuer without subjects', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerY.getAddress();
      const issuerName = 'Issuer Test';
      const issuerPublicKey = getPublicKeyFromAddress(issuerAddress);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .newIssuer(issuerAddress, issuerName, issuerPublicKey, []),
      ).revertedWithCustomError(
        fixture.issuersRegistry,
        'IssuerWithoutSubjects',
      );
    });

    it('Should revert if an non TEAM_ROLE account register an Issuer', async function () {
      const issuerAddress = await fixture.accountC.getAddress();
      const issuerName = 'Issuer Test';
      const issuerPublicKey = getPublicKeyFromAddress(issuerAddress);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.accountB)
          .newIssuer(issuerAddress, issuerName, issuerPublicKey, SUBJECTS),
      ).to.revertedWith(
        `AccessControl: account ${(
          await fixture.accountB.getAddress()
        ).toLowerCase()} is missing role ${fixture.TEAM_ROLE}`,
      );
    });
  });
}
