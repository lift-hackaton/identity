import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import {
  deployIssuerRegistryFixture,
  IIssuerRegistryFixture,
} from "../shared/fixtureIssuerRegistry";

const SUBJECTS = ["cpf", "name", "birthDate"];

export async function issuersRegistrySuspend(): Promise<void> {
  context("#suspendIssuer", async () => {
    let fixture: IIssuerRegistryFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployIssuerRegistryFixture);
    });

    it("Should suspend a registered issuer", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .suspendIssuer(issuerAddress, dateTimeSuspension)
      )
        .to.emit(fixture.issuersRegistry, "Suspended")
        .withArgs(issuerAddress, dateTimeSuspension);

      const issuerData = await fixture.issuersRegistry.getIssuer(issuerAddress);
      expect(issuerData.issuer).to.be.equal(issuerAddress);
      expect(issuerData.dateTimeSuspension).to.be.equal(dateTimeSuspension);
    });

    it("Should revert if try suspend a not registered Issuer", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.accountA.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .suspendIssuer(issuerAddress, dateTimeSuspension)
      ).to.revertedWithCustomError(fixture.issuersRegistry, "InexistentIssuer");
    });

    it("Should revert if try suspend a Issuer with a future date/time", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000) + 60;

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .suspendIssuer(issuerAddress, dateTimeSuspension)
      ).to.revertedWith("Date/Time suspension can't be future");
    });

    it("Should revert if try suspend an already suspended Issuer", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);
      await fixture.issuersRegistry
        .connect(fixture.teamMemberA)
        .suspendIssuer(issuerAddress, dateTimeSuspension);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .suspendIssuer(issuerAddress, dateTimeSuspension)
      ).to.revertedWithCustomError(fixture.issuersRegistry, "SuspendedIssuer");
    });

    it("Should revert if an non TEAM_ROLE account suspend an Issuer", async function () {
      //o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.accountB)
          .suspendIssuer(issuerAddress, dateTimeSuspension)
      ).to.revertedWith(
        `AccessControl: account ${(
          await fixture.accountB.getAddress()
        ).toLowerCase()} is missing role ${fixture.TEAM_ROLE}`
      );
    });
  });
}
