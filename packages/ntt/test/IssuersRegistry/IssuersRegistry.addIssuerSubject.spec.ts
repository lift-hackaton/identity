import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import {
  deployIssuerRegistryFixture,
  IIssuerRegistryFixture,
} from '../shared/fixtureIssuerRegistry';

const SUBJECTS = ['cpf', 'name', 'birthDate'];

export async function issuersRegistryAddSubject(): Promise<void> {
  context('#addIssuerSubject', async () => {
    let fixture: IIssuerRegistryFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployIssuerRegistryFixture);
    });

    it('Should add a subject to a registered issuer', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const newSubject = 'cnhTest';

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .addIssuerSubject(issuerAddress, newSubject),
      )
        .to.emit(fixture.issuersRegistry, 'SubjectsChanged')
        .withArgs(issuerAddress, fixture.issuerXSubjects.concat(newSubject));

      const issuerData = await fixture.issuersRegistry.getIssuer(issuerAddress);
      expect(issuerData.issuer).to.be.equal(issuerAddress);
      expect(issuerData.subjects).to.be.an('array').length(3);
      expect(issuerData.subjects[0]).to.be.equal('cpf');
      expect(issuerData.subjects[1]).to.be.equal('name');
      expect(issuerData.subjects[2]).to.be.equal(newSubject);
      expect(issuerData.dateTimeSuspension).to.be.equal(0);
    });

    it('Should revert if try add a subject to a not registered Issuer', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.accountA.getAddress();
      const newSubject = 'cnhTest';

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .addIssuerSubject(issuerAddress, newSubject),
      ).to.revertedWithCustomError(fixture.issuersRegistry, 'InexistentIssuer');
    });

    it('Should revert if try add a subject to a suspended Issuer', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const newSubject = 'cnhTest';
      const dateTimeSuspension = Math.floor(new Date().getTime() / 1000);
      await fixture.issuersRegistry
        .connect(fixture.teamMemberA)
        .suspendIssuer(issuerAddress, dateTimeSuspension);

      await expect(
        fixture.issuersRegistry
          .connect(fixture.teamMemberA)
          .addIssuerSubject(issuerAddress, newSubject),
      ).to.revertedWithCustomError(fixture.issuersRegistry, 'SuspendedIssuer');
    });

    it('Should revert if an non TEAM_ROLE account add a subject to an Issuer', async function () {
      // o fixture já registrou a conta issuerX
      const issuerAddress = await fixture.issuerX.getAddress();
      const newSubject = 'cnhTest';

      await expect(
        fixture.issuersRegistry
          .connect(fixture.accountB)
          .addIssuerSubject(issuerAddress, newSubject),
      ).to.revertedWith(
        `AccessControl: account ${(
          await fixture.accountB.getAddress()
        ).toLowerCase()} is missing role ${fixture.TEAM_ROLE}`,
      );
    });
  });
}
