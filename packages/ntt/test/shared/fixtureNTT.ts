import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { BigNumber, Signer } from 'ethers';
import { ethers } from 'hardhat';
import { IdentityNTT } from '../../typechain-types';
import { IssuersRegistry } from '../../typechain-types/contracts/IssuersRegistry';
import { getPublicKeyFromAddress } from './hardhatNetwork';

/**
 * @description Sets a initial state desired to every test
 *
 * Deploy NTT, associate account owner as DEFAULT_ADMIN_ROLE, manager as MANAGER_ROLE,
 * teamMemberA and teamMemberB as TEAM_ROLE. Mint a unexpireable NTT ID 1 and
 * a expired NTT ID 2 to accountE
 */
export async function deployNTTFixture(): Promise<IIdentityNTTFixture> {
  const IssuersRegistryFactory = await ethers.getContractFactory(
    'IssuersRegistry',
  );

  // Contracts are deployed using the first signer/account by default
  const [
    owner,
    manager,
    teamMemberA,
    teamMemberB,
    accountA,
    accountB,
    accountC,
    accountD,
    accountE,
    issuerX,
    issuerY,
  ] = await ethers.getSigners();

  const issuersRegistry = await getIssuerRegistry(
    manager,
    teamMemberA,
    issuerX,
    issuerY,
  );
  const IdentityNTTFactory = await ethers.getContractFactory('IdentityNTT');
  const identityNTT = await IdentityNTTFactory.deploy(
    issuersRegistry.address,
    'Registro Nacional de Pessoas Físicas e Jurídicas',
    'CPF/CNPJ',
  );

  // const DEFAULT_ADMIN_ROLE = await identityNTT.DEFAULT_ADMIN_ROLE();
  // const MANAGER_ROLE = await identityNTT.MANAGER_ROLE();
  // const TEAM_ROLE = await identityNTT.TEAM_ROLE();
  // // assign team members
  // await identityNTT.connect(manager).grantRole(TEAM_ROLE, teamMemberA.address);
  // await identityNTT.connect(manager).grantRole(TEAM_ROLE, teamMemberB.address);
  // mint a valid NTT to accountE
  await identityNTT
    .connect(issuerX)
    .mint(await accountE.getAddress(), `{"CPF":"signXPTO", "nome": "signXYZ"}`);

  // mint NTT to accountE and revoke
  await identityNTT
    .connect(issuerX)
    .mint(await accountE.getAddress(), `{"CPF":"signXPTO", "nome": "signXYZ"}`);
  // catching Minted event
  const filter = identityNTT.filters.Minted();
  const events = await identityNTT.queryFilter(filter);

  const credentialID = events[0].args.tokenId;
  const revokedCredentialID = events[1].args.tokenId;

  await identityNTT.connect(issuerX).revoke(revokedCredentialID);

  return {
    identityNTT,
    owner,
    manager,
    teamMemberA,
    teamMemberB,
    accountA,
    accountB,
    accountC,
    accountD,
    accountE,
    issuerX,
    issuerY,
    // DEFAULT_ADMIN_ROLE,
    // MANAGER_ROLE,
    // TEAM_ROLE,
    credentialID,
    expiredCredentialID: ethers.constants.Two,
    revokedCredentialID,
  };
}

async function getIssuerRegistry(
  manager: SignerWithAddress,
  teamMemberA: SignerWithAddress,
  issuerX: SignerWithAddress,
  issuerY: SignerWithAddress,
): Promise<IssuersRegistry> {
  const IssuersRegistryFactory = await ethers.getContractFactory(
    'IssuersRegistry',
  );
  const issuersRegistry = await IssuersRegistryFactory.deploy(manager.address);
  const TEAM_ROLE = await issuersRegistry.TEAM_ROLE();
  const ISSUERS_SUBJECTS = ['cpf', 'name'];
  // assign team member
  await issuersRegistry
    .connect(manager)
    .grantRole(TEAM_ROLE, teamMemberA.address);

  // register IssuerX
  await issuersRegistry
    .connect(teamMemberA)
    .newIssuer(
      issuerX.address,
      'Issuer X Unit Tests NTT S/A',
      getPublicKeyFromAddress(issuerX.address),
      ISSUERS_SUBJECTS,
    );

  // register IssuerY
  await issuersRegistry
    .connect(teamMemberA)
    .newIssuer(
      issuerY.address,
      'Issuer Y Unit Tests NTT S/A',
      getPublicKeyFromAddress(issuerY.address),
      ISSUERS_SUBJECTS,
    );

  return issuersRegistry;
}

export type IIdentityNTTFixture = {
  identityNTT: IdentityNTT;
  owner: Signer;
  manager: Signer;
  teamMemberA: Signer;
  teamMemberB: Signer;
  accountA: Signer;
  accountB: Signer;
  accountC: Signer;
  accountD: Signer;
  accountE: Signer;
  issuerX: Signer;
  issuerY: Signer;
  // DEFAULT_ADMIN_ROLE: string;
  // MANAGER_ROLE: string;
  // TEAM_ROLE: string;
  // a NTT ID minted in the fixture
  credentialID: BigNumber;
  expiredCredentialID: BigNumber;
  revokedCredentialID: BigNumber;
};
