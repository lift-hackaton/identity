import { Signer } from 'ethers';
import { ethers } from 'hardhat';
import { IssuersRegistry } from '../../typechain-types';
import { getPublicKeyFromAddress } from './hardhatNetwork';

/**
 * @description Sets a initial state desired to every test
 * Deploy Issuer Registry, associate account owner as DEFAULT_ADMIN_ROLE, manager as MANAGER_ROLE,
 * teamMemberA and teamMemberB as TEAM_ROLE. Register an Issuer with subjects "cpf" and "name"
 */
export async function deployIssuerRegistryFixture(): Promise<IIssuerRegistryFixture> {
  // Contracts are deployed using the first signer/account by default
  const [
    owner,
    manager,
    teamMemberA,
    teamMemberB,
    accountA,
    accountB,
    accountC,
    accountD,
    accountE,
    issuerX,
    issuerY,
  ] = await ethers.getSigners();

  const IssuersRegistryFactory = await ethers.getContractFactory(
    'IssuersRegistry',
  );
  const issuersRegistry = await IssuersRegistryFactory.deploy(manager.address);

  const DEFAULT_ADMIN_ROLE = await issuersRegistry.DEFAULT_ADMIN_ROLE();
  const MANAGER_ROLE = await issuersRegistry.MANAGER_ROLE();
  const TEAM_ROLE = await issuersRegistry.TEAM_ROLE();
  const ISSUERX_SUBJECTS = ['cpf', 'name'];
  // assign team members
  await issuersRegistry
    .connect(manager)
    .grantRole(TEAM_ROLE, teamMemberA.address);

  await issuersRegistry
    .connect(manager)
    .grantRole(TEAM_ROLE, teamMemberB.address);
  // register an Issuer
  const issuerAddress = await issuerX.getAddress();
  await issuersRegistry
    .connect(teamMemberA)
    .newIssuer(
      issuerAddress,
      'Unit Tests Issuer Registry S/A',
      getPublicKeyFromAddress(issuerAddress),
      ISSUERX_SUBJECTS,
    );

  return {
    issuersRegistry,
    owner,
    manager,
    teamMemberA,
    teamMemberB,
    accountA,
    accountB,
    accountC,
    accountD,
    accountE,
    DEFAULT_ADMIN_ROLE,
    MANAGER_ROLE,
    TEAM_ROLE,
    issuerX,
    issuerXSubjects: ISSUERX_SUBJECTS,
    issuerY,
  };
}

export type IIssuerRegistryFixture = {
  issuersRegistry: IssuersRegistry;
  owner: Signer;
  manager: Signer;
  teamMemberA: Signer;
  teamMemberB: Signer;
  accountA: Signer;
  accountB: Signer;
  accountC: Signer;
  accountD: Signer;
  accountE: Signer;
  DEFAULT_ADMIN_ROLE: string;
  MANAGER_ROLE: string;
  TEAM_ROLE: string;
  // issuer registered
  issuerX: Signer;
  issuerXSubjects: string[];
  // issuer not registered yet
  issuerY: Signer;
};
