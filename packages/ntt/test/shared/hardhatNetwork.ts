/**
 * Features to deal with Hardhat Network: npx hardhat node
 */
import { HDNode } from "ethers/lib/utils";
///****** */

const HARDHAT_NETWORK_MNEMONIC =
  "test test test test test test test test test test test junk";
const QUANTITY_ACCOUNTS = 20;

const HDWallet = HDNode.fromMnemonic(HARDHAT_NETWORK_MNEMONIC);
const walletCache: { [address: string]: HDNode } = {};

export function getPublicKeyFromAddress(address: string) {
  if (!walletCache[address]) {
    for (let i = 0; i < QUANTITY_ACCOUNTS; i++) {
      const wallet = HDWallet.derivePath(`m/44'/60'/0'/0/${i}`);
      if (!walletCache[wallet.address]) {
        walletCache[wallet.address] = wallet;
      }
      if (wallet.address == address) {
        break;
      }
    }
  }
  // EXISTE UMA VERSÃO COMPLETA, que começa com 0X04 e tem 65 bytes (130 nibbles), E UMA
  // VERSÃO COMPRIMIDA DE PUBLICKEY O HDNODE.publicKey DEVOLVE A VERSÃO COMPRIMIDA:
  // https://docs.ethers.io/v5/api/utils/hdnode/

  // console.log(walletCache[address].extendedKey);
  // console.log("publicKey", walletCache[address].publicKey);

  // const privateKeyBuffer = toBuffer(walletCache[address].privateKey);
  // const wallet = Wallet.fromPrivateKey(privateKeyBuffer);
  // const publicKey = wallet.getPublicKeyString();
  // console.log(`PublicKey: ${publicKey}`);
  // // os valores abaixo da ethersjs não batem com os da biblioteca ethereumjs-wallet
  // console.log(
  //   `ethersjs compressed: `,
  //   ethers.utils.computePublicKey(walletCache[address].privateKey, true)
  // );
  // console.log(
  //   `ethersjs uncompressed: `,
  //   ethers.utils.computePublicKey(walletCache[address].privateKey, false)
  // );

  return walletCache[address].publicKey;
}
