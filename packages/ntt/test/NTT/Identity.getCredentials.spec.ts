import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";
import { ICredential } from "../../model/ICredential";

export async function identityGetCredentials(): Promise<void> {
  context("#getCredentials", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should return all NTTs Credentials", async function () {
      const ownerAddress = await fixture.accountE.getAddress();
      const credentials: ICredential[] =
        await fixture.identityNTT.getCredentials(ownerAddress);
      expect(credentials).to.be.an("array").with.length(2);
      //VALID CREDENTIAL
      expect(credentials[0].owner).to.be.equal(ownerAddress);
      expect(credentials[0].issuer).to.be.equal(
        await fixture.issuerX.getAddress()
      );
      expect(credentials[0].valid).to.be.true;
      //INVALID CREDENTIAL
      expect(credentials[1].owner).to.be.equal(ownerAddress);
      expect(credentials[1].issuer).to.be.equal(
        await fixture.issuerX.getAddress()
      );
      expect(credentials[1].valid).to.be.false;
    });
    it("Should return empty array if there is no credentials for informed address", async function () {
      const ownerAddress = await fixture.accountA.getAddress();
      const credentials: ICredential[] =
        await fixture.identityNTT.getCredentials(ownerAddress);
      expect(credentials).to.be.an("array").with.length(0);
    });
  });
}
