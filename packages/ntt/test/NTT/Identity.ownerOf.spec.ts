import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";
import { UtilsTest } from "../shared/Utils";

export async function identityOwnerOf(): Promise<void> {
  context("#ownerOf", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should return ownerOf NTT", async function () {
      const tokenId = fixture.credentialID;
      expect(await fixture.identityNTT.ownerOf(tokenId)).to.be.equal(
        await fixture.accountE.getAddress()
      );
    });
    it("Should revert if get ownerOf inexistent credentialID", async function () {
      const credentialID = UtilsTest.getRandomAmount(1000);
      await expect(fixture.identityNTT.ownerOf(credentialID)).to.revertedWith(
        "ERC4671: Token does not exist"
      );
    });
  });
}
