import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";

export async function passportDeployment(): Promise<void> {
  context("#deployment", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should revert if inform a address that is not a IRegistry", async function () {
      const IdentityNTTFactory = await ethers.getContractFactory("IdentityNTT");
      const identityNTT = await IdentityNTTFactory.deploy(
        await fixture.accountA.getAddress(),
        "Registro Nacional de Pessoas Físicas e Jurídicas",
        "CPF/CNPJ"
      );
      //TODO: A exceção deveria acontecer na verdade na publicação. Ver isso depois
      expect(
        identityNTT
          .connect(fixture.issuerX)
          .mint(
            await fixture.accountA.getAddress(),
            `{"CPF":"signXPTO", "nome": "signXYZ"}`
          )
      ).to.be.rejectedWith(
        "Transaction reverted: function call to a non-contract account"
      );
    });
  });
}
