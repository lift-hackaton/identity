import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ICredential } from "../../model/ICredential";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";
import { UtilsTest } from "../shared/Utils";

export async function identityRevoke(): Promise<void> {
  context("#revoke", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should revoke an existing not revoked credential ID", async function () {
      const holderAddress = await fixture.accountE.getAddress();
      const tokenId = fixture.credentialID;

      await expect(fixture.identityNTT.connect(fixture.issuerX).revoke(tokenId))
        .to.emit(fixture.identityNTT, "Revoked")
        .withArgs(holderAddress, tokenId);
      const credential: ICredential = await fixture.identityNTT.getCredential(
        tokenId
      );
      //expect(credential.tokenId).to.be.equal(tokenId);
      expect(credential.owner).to.be.equal(holderAddress);
      expect(credential.issuer).to.be.equal(await fixture.issuerX.getAddress());
      expect(credential.valid).to.be.false;
    });
    it("Should revert if revoke an inexistent Credential", async function () {
      const credentialID = UtilsTest.getRandomAmount() + 1000;
      await expect(
        fixture.identityNTT.connect(fixture.issuerX).revoke(credentialID)
      ).to.revertedWith("ERC4671: Token does not exist");
    });

    it("Should revert if revoke a credential ID already revoked", async function () {
      const credentialID = fixture.revokedCredentialID;
      await expect(
        fixture.identityNTT.connect(fixture.issuerX).revoke(credentialID)
      ).to.revertedWith("Token is already invalid");
    });
    it("Should revert if revoke a credential ID issued by another Issuer", async function () {
      const credentialID = fixture.credentialID;
      await expect(
        fixture.identityNTT.connect(fixture.issuerY).revoke(credentialID)
      ).to.revertedWith(
        "Unauthorized. Only the token's Issuer is able to revoke it"
      );
    });
    it("Should revert if an non TEAM_ROLE account revoke a credential ID", async function () {
      const credentialID = fixture.credentialID;

      await expect(
        fixture.identityNTT.connect(fixture.accountB).revoke(credentialID)
      ).to.revertedWith(
        "Unauthorized. Only registered issuers can execute this operation"
      );
    });
  });
}
