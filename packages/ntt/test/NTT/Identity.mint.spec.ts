import { loadFixture, time } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { BigNumber } from "ethers";
import { ethers } from "hardhat";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";
import { ICredential } from "../../model/ICredential";
import { UtilsTest } from "../shared/Utils";

export async function identityMint(): Promise<void> {
  context("#mint", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should mint a credential ID that was not minted yet", async function () {
      const holderAddress = await fixture.accountA.getAddress();
      const credentialID = 2; //o fixture já mintou os tokenId 0 e 1

      await expect(
        fixture.identityNTT
          .connect(fixture.issuerX)
          .mint(holderAddress, `{"CPF":"signXPTO", "nome": "signXYZ"}`)
      )
        .to.emit(fixture.identityNTT, "Minted")
        .withArgs(holderAddress, credentialID);
      expect(await fixture.identityNTT.ownerOf(credentialID)).to.be.equal(
        holderAddress
      );
      expect(await fixture.identityNTT.balanceOf(holderAddress)).to.be.equal(
        ethers.constants.One
      );
      const credential: ICredential = await fixture.identityNTT.getCredential(
        credentialID
      );
      //expect(credential.tokenId).to.be.equal(credentialID);
      expect(credential.owner).to.be.equal(holderAddress);
      expect(credential.issuer).to.be.equal(await fixture.issuerX.getAddress());
      // expect(credential.issueDate).to.be.equal(
      //   BigNumber.from(await time.latest())
      // );
      expect(credential.valid).to.be.true;
    });
    it("Should revert if mint to a zero address holder", async function () {
      const credentialID = UtilsTest.getRandomAmount();
      await expect(
        fixture.identityNTT
          .connect(fixture.issuerX)
          .mint(
            ethers.constants.AddressZero,
            `{"CPF":"signXPTO", "nome": "signXYZ"}`
          )
      ).to.revertedWith("ERC4671: mint to the zero address");
    });
    // it("Should revert if mint a credential ID already minted to the same address", async function () {
    //   const holderAddress = await fixture.accountE.getAddress();
    //   const credentialID = fixture.credentialID;
    //   await expect(
    //     fixture.identityNTT
    //       .connect(fixture.teamMemberA)
    //       .mint(holderAddress)
    //   ).to.revertedWith("Cannot mint an assigned token");
    // });
    // it("Should revert if mint a credential ID already minted to another address", async function () {
    //   const holderAddress = await fixture.accountA.getAddress();
    //   const credentialID = fixture.credentialID;
    //   await expect(
    //     fixture.identityNTT
    //       .connect(fixture.teamMemberA)
    //       .mint(holderAddress)
    //   ).to.revertedWith("Cannot mint an assigned token");
    // });

    it("Should revert if an account not registered in the IssuersRegistry contract try to mint a NTT", async function () {
      const holderAddress = await fixture.accountA.getAddress();
      const credentialID = UtilsTest.getRandomAmount();

      await expect(
        fixture.identityNTT
          .connect(fixture.accountB)
          .mint(holderAddress, `{"CPF":"signXPTO", "nome": "signXYZ"}`)
      ).to.revertedWith(
        `Unauthorized. Only registered issuers can execute this operation`
      );
    });
  });
}
