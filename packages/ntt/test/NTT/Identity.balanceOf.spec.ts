import { BigNumber } from "ethers";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";
import { UtilsTest } from "../shared/Utils";

export async function identityBalanceOf(): Promise<void> {
  context("#balanceOf", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should return balanceOf multi NTT owner", async function () {
      const holderAddress = await fixture.accountE.getAddress();
      expect(await fixture.identityNTT.balanceOf(holderAddress)).to.be.equal(
        BigNumber.from(2)
      );
    });
    it("Should return balanceOf only one NTT owner", async function () {
      const holderAddress = await fixture.accountA.getAddress();

      await fixture.identityNTT
        .connect(fixture.issuerX)
        .mint(holderAddress, `{"CPF":"signXPTO", "nome": "signXYZ"}`);

      expect(await fixture.identityNTT.balanceOf(holderAddress)).to.be.equal(
        ethers.constants.One
      );
    });
    it("Should return balanceOf account without NTT", async function () {
      const holderAddress = await fixture.accountA.getAddress();
      expect(await fixture.identityNTT.balanceOf(holderAddress)).to.be.equal(
        ethers.constants.Zero
      );
    });
    it("Should revert if gets balance of a zero address holder", async function () {
      await expect(
        fixture.identityNTT
          .connect(fixture.teamMemberA)
          .balanceOf(ethers.constants.AddressZero)
      ).to.revertedWith("ERC4671: address zero is not a valid owner");
    });
  });
}
