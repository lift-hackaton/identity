import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { deployNTTFixture, IIdentityNTTFixture } from "../shared/fixtureNTT";
import { ICredential } from "../../model/ICredential";
import { UtilsTest } from "../shared/Utils";

export async function identityGetCredential(): Promise<void> {
  context("#getCredential", async () => {
    let fixture: IIdentityNTTFixture;
    beforeEach(async function () {
      // We use loadFixture to run this setup once, snapshot that state,
      // and reset Hardhat Network to that snapshopt in every test.
      fixture = await loadFixture(deployNTTFixture);
    });

    it("Should return NTT Credential", async function () {
      const tokenId = fixture.credentialID;
      const credential: ICredential = await fixture.identityNTT.getCredential(
        tokenId
      );
      //expect(credential.tokenId).to.be.equal(tokenId);
      expect(credential.owner).to.be.equal(await fixture.accountE.getAddress());
      expect(credential.issuer).to.be.equal(await fixture.issuerX.getAddress());
      expect(credential.valid).to.be.true;
    });
    it("Should return revoked NTT Credential", async function () {
      const tokenId = fixture.revokedCredentialID;
      const credential: ICredential = await fixture.identityNTT.getCredential(
        tokenId
      );
      //expect(credential.tokenId).to.be.equal(tokenId);
      expect(credential.owner).to.be.equal(await fixture.accountE.getAddress());
      expect(credential.issuer).to.be.equal(await fixture.issuerX.getAddress());
      expect(credential.valid).to.be.false;
    });

    it("Should revert if get inexistent Credential", async function () {
      const credentialID = UtilsTest.getRandomAmount() + 1000;
      await expect(
        fixture.identityNTT.getCredential(credentialID)
      ).to.revertedWith("ERC4671: Token does not exist");
    });
  });
}
