import { issuersRegistryAddSubject } from "./IssuersRegistry/IssuersRegistry.addIssuerSubject.spec";
import { issuersRegistryDeployment } from "./IssuersRegistry/IssuersRegistry.deployment.spec";
import { issuersRegistryGetIssuer } from "./IssuersRegistry/IssuersRegistry.getIssuer.spec";
import { issuersRegistryGetIssuers } from "./IssuersRegistry/IssuersRegistry.getIssuers.spec";
import { issuersRegistryIsValid } from "./IssuersRegistry/IssuersRegistry.isValid.spec";
import { issuersRegistryNew } from "./IssuersRegistry/IssuersRegistry.newIssuer.spec";
import { issuersRegistrySuspend } from "./IssuersRegistry/IssuersRegistry.suspendIssuer.spec";
import { identityBalanceOf } from "./NTT/Identity.balanceOf.spec";
import { identityGetCredential } from "./NTT/Identity.getCredential.spec";
import { identityGetCredentials } from "./NTT/Identity.getCredentials.spec";
import { identityHasValid } from "./NTT/Identity.hasValid.spec";
import { identityIsValid } from "./NTT/Identity.isValid.spec";
import { identityMint } from "./NTT/Identity.mint.spec";
import { identityOwnerOf } from "./NTT/Identity.ownerOf.spec";
import { identityRevoke } from "./NTT/Identity.revoke.spec";
import { passportDeployment } from "./NTT/IdentityNTT.deployment.spec";

describe(`Integration tests`, async () => {
  describe(`Identity NTT`, async () => {
    passportDeployment();
    identityBalanceOf();
    identityMint();
    identityOwnerOf();
    identityGetCredential();
    identityGetCredentials();
    identityRevoke();
    identityIsValid();
    identityHasValid();
  });
  describe(`Issuers Registry`, async () => {
    issuersRegistryDeployment();
    issuersRegistryGetIssuer();
    issuersRegistryIsValid();
    issuersRegistryGetIssuers();
    issuersRegistryNew();
    issuersRegistrySuspend();
    issuersRegistryAddSubject();
  });
});
