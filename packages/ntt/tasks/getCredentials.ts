import { ICredential } from './../model/ICredential';
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Result } from "ethers/lib/utils";
import { task } from "hardhat/config";
import { getDeployedContract } from "../utils/helpers";

task(
  "getCredentials",
  "Get the metadata of all NTTs already minted for a specific account"
)
  .addParam("owner", "The account address the NTT is being minted to")
  .setAction(async (taskArgs, hre) => {
    // Esses imports dinâmicos são necessários pois durante a inicialização do hardhat
    // não é permitido importar o módulo 'hardhat' nem nenhum módulo que o importe
    const { appConfig } = await import("../utils/helpers");

    const CONTRACT_NAME: string = Object.keys(appConfig.contract)[0];

    const identityNTT = await getDeployedContract(
      hre.network.name,
      CONTRACT_NAME
    );
    const credentials = await identityNTT.getCredentials(taskArgs.owner);

    const credentialsFinal = credentials.map((c: ICredential) => {
      let dataObj = {};
      try{
        dataObj = JSON.parse(c.data);
      }catch(e: unknown){
        console.log(`Parse failed for data '${c.data}': ${(<Error>e).message}`);
      }
      return {... dataObj, issuer: c.issuer, owner: c.owner, valid: c.valid };
    });
    console.log(credentialsFinal);
  });
