import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { Result } from 'ethers/lib/utils';
import { task } from 'hardhat/config';
import { getDeployedContract } from '../utils/helpers';

task('registerIssuer', 'Register an Issuer in the Issuers Registry Contract')
  .addParam(
    'teammemberaddress',
    'The account address responsible for sendint the mint transaction',
  )
  .addParam('issuer', `The Issuer's account address being registered`)
  .addParam('issuername', `The Issuer's name being registered`)
  .addParam('publickey', `The Issuer's public key being registered`)
  .addParam(
    'subjects',
    'Subjects the Issuer is authorized to issue separated by comma',
    'cpf,name',
  )
  .setAction(async (taskArgs, hre) => {
    // Esses imports dinâmicos são necessários pois durante a inicialização do hardhat
    // não é permitido importar o módulo 'hardhat' nem nenhum módulo que o importe
    const { appConfig } = await import('../utils/helpers');
    const { filterSignerByAddress, getSigners } = await import('../utils');

    const CONTRACT_NAME: string = Object.keys(appConfig.contract)[1];

    const signers: SignerWithAddress[] =
      (await getSigners()) as SignerWithAddress[];
    const teamMemberSigner = await filterSignerByAddress(
      signers,
      taskArgs.teammemberaddress,
    );
    if (!teamMemberSigner) {
      throw new Error(
        `The Team Member account informed, ${taskArgs.teammemberaddress}, is not included among signers`,
      );
    }

    const issuersRegistryContrat = await getDeployedContract(
      hre.network.name,
      CONTRACT_NAME,
      teamMemberSigner,
    );

    console.log(taskArgs.subjects, taskArgs.subjects.split(','));
    await issuersRegistryContrat.newIssuer(
      taskArgs.issuer,
      taskArgs.issuername,
      taskArgs.publickey,
      taskArgs.subjects.split(','),
    );

    // catching Registered event
    const filter = issuersRegistryContrat.filters.Registered();
    const events = await issuersRegistryContrat.queryFilter(filter);

    // Faz um for de trás pra frente para pegar os mais recentes
    for (let i = events.length - 1; i >= 0; i--) {
      if ((events[i].args as Result).issuer === taskArgs.issuer) {
        console.log(`Issuer ${(events[i].args as Result).issuer} registered`);
        break;
      }
    }
  });
