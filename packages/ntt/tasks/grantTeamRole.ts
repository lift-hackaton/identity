import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { task } from 'hardhat/config';
import { getDeployedContract } from '../utils/helpers';

task(
  'grantTeamRole',
  'Grant the role of TEAM_MEMBER of IssuersRegistry Contract to a specific address',
)
  .addParam(
    'teammemberaddress',
    'The account address that will be granted with TEAM_MEMBER role',
  )
  .setAction(async (taskArgs, hre) => {
    // Esses imports dinâmicos são necessários pois durante a inicialização do hardhat
    // não é permitido importar o módulo 'hardhat' nem nenhum módulo que o importe
    const { appConfig } = await import('../utils/helpers');
    const { filterSignerByAddress, getSigners } = await import('../utils');

    const CONTRACT_NAME: string = Object.keys(appConfig.contract)[1];
    const MANAGER_ADDRESS = appConfig.contract[CONTRACT_NAME][0].args.manager;

    const signers: SignerWithAddress[] =
      (await getSigners()) as SignerWithAddress[];
    const managerSigner = await filterSignerByAddress(
      signers,
      MANAGER_ADDRESS as string,
    );
    if (!managerSigner) {
      throw new Error(
        `The manager account set in config.yml, ${MANAGER_ADDRESS}, is not included among signers`,
      );
    }

    const identityNTT = await getDeployedContract(
      hre.network.name,
      CONTRACT_NAME,
      managerSigner,
    );
    const TEAM_ROLE = await identityNTT.TEAM_ROLE();
    await identityNTT.grantRole(TEAM_ROLE, taskArgs.teammemberaddress);
    console.log(`TEAM_MEMBER role granted to ${taskArgs.teammemberaddress}`);
  });
