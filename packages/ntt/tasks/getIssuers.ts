import { task } from 'hardhat/config';
import { getDeployedContract } from '../utils/helpers';

task(
  'getIssuers',
  'List all registered Issuers in the Issuers Registry Contract',
).setAction(async (taskArgs, hre) => {
  // Esses imports dinâmicos são necessários pois durante a inicialização do hardhat
  // não é permitido importar o módulo 'hardhat' nem nenhum módulo que o importe
  const { appConfig } = await import('../utils/helpers');

  const CONTRACT_NAME: string = Object.keys(appConfig.contract)[1];

  const issuersRegistryContrat = await getDeployedContract(
    hre.network.name,
    CONTRACT_NAME,
  );

  const issuers = await issuersRegistryContrat.getIssuers();

  console.log(issuers);
});
