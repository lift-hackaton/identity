import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { Result } from 'ethers/lib/utils';
import { task } from 'hardhat/config';
import { getDeployedContract } from '../utils/helpers';

task(
  'mint',
  'Mint a NTT for a specific account address with the credential data informed',
)
  .addParam(
    'teammemberaddress',
    'The account address responsible for sendint the mint transaction',
  )
  .addParam('owner', 'The account address the NTT is being minted to')
  .addParam(
    'data',
    'Credential JSON metadata',
    `{"cpf": "00000000000", "name": "Edson Arantes do Nascimento", "birthDate": "1940-10-23","birthCity":"Três Corações-MG", "country": "Brazil"}`,
  )
  .setAction(async (taskArgs, hre) => {
    // Esses imports dinâmicos são necessários pois durante a inicialização do hardhat
    // não é permitido importar o módulo 'hardhat' nem nenhum módulo que o importe
    const { appConfig } = await import('../utils/helpers');
    const { filterSignerByAddress, getSigners } = await import('../utils');

    const CONTRACT_NAME: string = Object.keys(appConfig.contract)[0];

    const signers: SignerWithAddress[] =
      (await getSigners()) as SignerWithAddress[];
    const teamMemberSigner = await filterSignerByAddress(
      signers,
      taskArgs.teammemberaddress,
    );
    if (!teamMemberSigner) {
      throw new Error(
        `The team member account informed, ${taskArgs.teammemberaddress}, is not included among signers`,
      );
    }

    const identityNTT = await getDeployedContract(
      hre.network.name,
      CONTRACT_NAME,
      teamMemberSigner,
    );
    await identityNTT.mint(taskArgs.owner, taskArgs.data);

    // catching Minted event
    const filter = identityNTT.filters.Minted();
    const events = await identityNTT.queryFilter(filter);

    // Faz um for de trás pra frente para pegar os mais recentes
    for (let i = events.length - 1; i >= 0; i--) {
      if ((events[i].args as Result).owner === taskArgs.owner) {
        console.log(
          `NTT minted with ID ${(events[i].args as Result).tokenId} to ${
            (events[i].args as Result).owner
          } `,
        );
        break;
      }
    }
  });
