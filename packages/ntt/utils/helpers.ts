import fs from 'fs';
import { entropyToMnemonic, isValidMnemonic } from '@ethersproject/hdnode';
import { randomBytes } from '@ethersproject/random';
import * as yaml from 'js-yaml';
import * as dotenv from 'dotenv';

type Deploy = {
  value?: string | number | null;
  args: Record<string, string | number>;
  networks: Record<string, string> | any;
};

type Contract = Record<string, Deploy[]>;

type AppConfig = {
  env: Record<string, any>;
  ledger: {
    path: number;
    connect: boolean;
  };
  log: number;
  testNetwork: string;
  contract: Contract;
};

function omit(key: string, obj: Record<string, any>) {
  const { [key]: omitted, ...rest } = obj;
  return rest;
}

function getEnvVariable(key: string, defaultValue: any): any {
  if (process.env[key]) {
    return process.env[key];
  }

  if (!defaultValue) {
    throw `${key} is not defined and no default value was provided`;
  }
  return defaultValue;
}

function omitEnv(_config: AppConfig) {
  if (!_config.env) {
    _config.env = {};
  }

  // Force mnemonic
  if (!_config.env.MNEMONIC) {
    _config.env.MNEMONIC = '';
  }

  for (const variable in _config.env) {
    // eslint-disable-next-line no-prototype-builtins
    if (_config.env.hasOwnProperty(variable)) {
      _config.env[variable] = '';
    }
  }
  return _config;
}

function getConfig(): AppConfig {
  return yaml.load(fs.readFileSync('config.yml', 'utf8')) as AppConfig;
}

export const appConfig: AppConfig = omitEnv(getConfig());

export function setConfig(_config: AppConfig) {
  if (!_config.env) {
    _config.env = {};
  }

  for (const variable in _config.env) {
    // eslint-disable-next-line no-prototype-builtins
    if (_config.env.hasOwnProperty(variable)) {
      const env: string | undefined = process.env[variable]
        ? process.env[variable]
        : '';
      if (env && !_config.env[variable]) {
        _config.env[variable] = env;
      }
    }
  }

  fs.writeFile('config.yml', yaml.dump(_config), (err) => {
    if (err) {
      console.log(err);
    }
  });
}

function createMnemonic(): string {
  return entropyToMnemonic(randomBytes(32));
}

export function config() {
  dotenv.config();
  const _config: AppConfig = getConfig();
  if (!_config.env) {
    _config.env = {};
  }

  if (!_config.env.MNEMONIC && process.env.MNEMONIC) {
    _config.env.MNEMONIC = process.env.MNEMONIC;
  }

  // Force mnemonic
  if (!isValidMnemonic(_config.env.MNEMONIC)) {
    _config.env.MNEMONIC = createMnemonic();
    setConfig(_config);
  }

  for (const variable in _config.env) {
    // eslint-disable-next-line no-prototype-builtins
    if (_config.env.hasOwnProperty(variable)) {
      process.env[variable] = _config.env[variable];
    }
  }
}

export const logger = (type: number, msg: any) => {
  if (type <= appConfig.log) {
    console.log(msg);
  }
};

export function contructorArgs(construtor: Record<string, any>): any[] {
  const args = [];
  for (const arg in construtor) {
    // eslint-disable-next-line no-prototype-builtins
    if (construtor.hasOwnProperty(arg)) {
      args.push(construtor[arg]);
    }
  }
  return args;
}

export const getDeployedContract = async (
  networkName: string,
  contractName: string,
  _signer?: any,
) => {
  const { getContractFactory, getAccount } = await import('.');
  if (!appConfig.contract[contractName]) {
    throw new Error(
      `There is no data about '${contractName}' in file config.yml`,
    );
  }

  if (!appConfig.contract[contractName][0].networks[networkName]) {
    throw new Error(
      `There is no ${contractName}'s ${networkName} address in file config.yml`,
    );
  }
  const contractAddress =
    appConfig.contract[contractName][0].networks[networkName];

  const signer = typeof _signer !== 'undefined' ? _signer : await getAccount();
  logger(3, `Using contract address: ${contractAddress}`);
  const myContract = await getContractFactory(contractName, signer);
  return myContract.attach(contractAddress);
};
