
export interface ICredential {
  issuer: string;
  owner: string;
  data: string;
  // wallet address of the natural person who issued the credential (???)
  valid: boolean;
}
