/* eslint-disable camelcase */
import FlaskOnboarding from './onboarding';
import {
  toBufferbase64,
  encodeEncrypt,
  singleNumberArray,
  singleBuffer,
  fromSingleBuffer,
  personalPublicKey,
} from './encryption';

import { issuerApi, userApi } from './server';

import {
  isFlask,
  isSnapInstalled,
  wallet_getSnaps,
  wallet_enable,
  wallet_installSnaps,
  setState,
  getState,
  addState,
  createIdentity,
  getSubject,
  signId,
  signSubject,
  verifySubject,
  verifyId,
  getCredentials,
  saveCredentials,
  getIssuers,
  newIssuer,
  getEncryptionPublicKey,
  mint,
  credentials2identity,
} from './flask';

import './styles/index.css';

import {
  eth_chainId,
  net_version,
  eth_accounts,
  selectedAddress,
  eth_requestAccounts,
  connectAccounts,
  Web3WalletPermission,
  wallet_requestPermissions,
  wallet_getPermissions,
  wallet_switchEthereumChain,
  wallet_addEthereumChain,
  eth_getEncryptionPublicKey,
  eth_decrypt,
  personal_sign,
  personal_ecRecover,
  isConnected,
  isInstalled,
  isUnlocked,
  onConnect,
  onChainChanged,
  onAccountsChanged,
  numberToHex,
  stringToHex,
} from './metamask';

import {
  forwarderOrigin,
  snapOrigin,
  snapVersion,
  nttContractAddress,
  issuerContractAddress,
} from './config';

import {
  WalletGetSnapsResult,
  WalletEnableResult,
  WalletInstallSnapsResult,
} from './types';

const networkDiv = document.getElementById('network') as HTMLSpanElement;
const chainIdDiv = document.getElementById('chainId') as HTMLSpanElement;
const accountsDiv = document.getElementById('accounts') as HTMLSpanElement;
const permissionsResult = document.getElementById(
  'permissionsResult',
) as HTMLSpanElement;
const snapIdDiv = document.getElementById('snapid') as HTMLSpanElement;
const snapVersionDiv = document.getElementById(
  'snapversion',
) as HTMLSpanElement;
const isInstalledInput = document.getElementById(
  'isInstalled',
) as HTMLInputElement;
const isFlaskInput = document.getElementById('isFlask') as HTMLInputElement;
const isUnlockedInput = document.getElementById(
  'isUnlocked',
) as HTMLInputElement;
const isSnapInstalledInput = document.getElementById(
  'isSnapInstalled',
) as HTMLInputElement;

const onboardButton = document.getElementById(
  'connectButton',
) as HTMLButtonElement;
const switchEthereumChain = document.getElementById(
  'switchEthereumChain',
) as HTMLButtonElement;
const snapEnable = document.getElementById('snapEnable') as HTMLButtonElement;

const backuptext = document.getElementById('backuptext') as HTMLTextAreaElement;
const backupButton = document.getElementById('backup') as HTMLButtonElement;
const restoreButton = document.getElementById('restore') as HTMLButtonElement;
const restoretext = document.getElementById(
  'restoretext',
) as HTMLTextAreaElement;

const signedtext = document.getElementById('signedtext') as HTMLTextAreaElement;
const verifiedtext = document.getElementById('verifiedtext') as HTMLPreElement;
const signButton = document.getElementById('sign') as HTMLButtonElement;
const verifyButton = document.getElementById('verify') as HTMLButtonElement;
const cleartext = document.getElementById('cleartext') as HTMLTextAreaElement;

const addtext = document.getElementById('addtext') as HTMLTextAreaElement;
const identitycreated = document.getElementById(
  'identitycreated',
) as HTMLPreElement;
const gettext = document.getElementById('gettext') as HTMLTextAreaElement;
const identitystate = document.getElementById(
  'identitystate',
) as HTMLPreElement;
const identityVerification = document.getElementById(
  'identityVerification',
) as HTMLPreElement;
const createButton = document.getElementById('create') as HTMLButtonElement;
const addCreatedButton = document.getElementById(
  'addCreated',
) as HTMLButtonElement;
const getButton = document.getElementById('get') as HTMLButtonElement;
const verifySubjectButton = document.getElementById(
  'verifySubject',
) as HTMLButtonElement;

const useraddress = document.getElementById('useraddress') as HTMLInputElement;
const nttCredentials = document.getElementById(
  'nttCredentials',
) as HTMLPreElement;
const identityUpdated = document.getElementById(
  'identityUpdated',
) as HTMLPreElement;
const getCredentialsButton = document.getElementById(
  'getCredentials',
) as HTMLButtonElement;
const saveCredentialsButton = document.getElementById(
  'saveCredentials',
) as HTMLButtonElement;
const addUpdatedButton = document.getElementById(
  'addUpdated',
) as HTMLButtonElement;

const nttIssuers = document.getElementById('nttIssuers') as HTMLPreElement;
const getIssuersButton = document.getElementById(
  'getIssuers',
) as HTMLButtonElement;

const issuerAddress = document.getElementById(
  'issuerAddress',
) as HTMLInputElement;
const issuerName = document.getElementById('issuerName') as HTMLInputElement;
const issuerPublicKey = document.getElementById(
  'issuerPublicKey',
) as HTMLInputElement;
const issuerSubjects = document.getElementById(
  'issuerSubjects',
) as HTMLTextAreaElement;
const issuerData = document.getElementById('issuerData') as HTMLPreElement;
const getPublicKeyButton = document.getElementById(
  'getPublicKey',
) as HTMLButtonElement;
const newIssuerButton = document.getElementById(
  'newIssuer',
) as HTMLButtonElement;

const holderAddress = document.getElementById(
  'holderAddress',
) as HTMLInputElement;
const holderSubjects = document.getElementById(
  'holderSubjects',
) as HTMLTextAreaElement;
const issuerSignedSubjects = document.getElementById(
  'issuerSignedSubjects',
) as HTMLTextAreaElement;
const mintData = document.getElementById('mintData') as HTMLPreElement;
const signSubjectsButton = document.getElementById(
  'signSubjects',
) as HTMLButtonElement;
const mintButton = document.getElementById('mint') as HTMLButtonElement;

const sendIssuerData = document.getElementById(
  'sendIssuerData',
) as HTMLTextAreaElement;
const getIssuerData = document.getElementById(
  'getIssuerData',
) as HTMLPreElement;
const issuerPatchButton = document.getElementById(
  'issuerPatch',
) as HTMLButtonElement;
const issuerGetButton = document.getElementById(
  'issuerGet',
) as HTMLButtonElement;

const sendUserData = document.getElementById(
  'sendUserData',
) as HTMLTextAreaElement;
const getUserData = document.getElementById('getUserData') as HTMLPreElement;
const userPatchButton = document.getElementById(
  'userPatch',
) as HTMLButtonElement;
const userGetButton = document.getElementById('userGet') as HTMLButtonElement;

let accounts: string[] = [];

const main = async () => {
  const onboarding = new FlaskOnboarding({ forwarderOrigin });

  const onClickConnect = async () => {
    connectAccounts()
      .then((_accounts) => {
        accounts = _accounts;
      })
      .catch((error) => {
        if (error.code === 4001) {
          // EIP-1193 userRejectedRequest error
          console.log('Please connect to MetaMask.');
        } else {
          console.error(error);
        }
      });
  };

  const onClickInstall = () => {
    onboardButton.innerText = 'Onboarding in progress';
    onboardButton.disabled = true;
    onboarding.startOnboarding();
  };

  const initializeAccountButtons = () => {
    //
    switchEthereumChain.onclick = async () => {
      wallet_switchEthereumChain(numberToHex(80001))
        .then(async () => {
          await handleNetworkAndChainId();
        })
        .catch((error) => {
          if (error.code === 4902) {
            // This error code indicates that the chain has not been added to MetaMask.
            wallet_addEthereumChain({
              chainId: numberToHex(80001),
              rpcUrls: ['https://rpc-mumbai.maticvigil.com'],
              chainName: 'Mumbai Testnet',
              nativeCurrency: {
                name: 'tMATIC',
                symbol: 'tMATIC',
                decimals: 18,
              },
              blockExplorerUrls: ['https://mumbai.polygonscan.com/'],
            })
              .then(async () => {
                await handleNetworkAndChainId();
              })
              .catch((err) => {
                console.error(err);
              });
          } else {
            console.error(error);
          }
        });
    };

    snapEnable.onclick = async () => {
      console.log(`enabling: ${snapOrigin} , version: ${snapVersion}`);
      wallet_enable(snapOrigin, { version: snapVersion })
        .then(async (result: WalletEnableResult) => {
          // console.log(`Snap Enable result: ${JSON.stringify(result)}`);
          if (result.snaps[snapOrigin].error) {
            console.error(result.snaps[snapOrigin].error);
          } else {
            await handleAccount();
          }
        })
        .catch((error) => {
          if (error.code === 4001) {
            // EIP-1193 userRejectedRequest error
            console.log('Please connect to MetaMask Snap.');
          } else {
            console.error(error);
          }
        });
    };

    restoreButton.onclick = async () => {
      const id =
        '[{"version": 1,"subject": "cpf","encrypted": false,"data": "cpf.num.ero-xx","user": "0x9870...","issuer": "0x9870...","chainid": "8001","contract": "0x6259...","signature": "aquelehash"},{"version": 1,"subject": "nome","encrypted": false,"data": "meu nome completo","user": "0x9870...","issuer": "0x9870...","chainid": "8001","contract": "0x6259...","signature": "aquelehashdonome"}]';
      setState(JSON.parse(restoretext.value))
        .then((result) => {
          console.log(JSON.stringify(result));
        })
        .catch((error) => {
          console.error(error);
        });
    };

    backupButton.onclick = async () => {
      getState()
        .then((result) => {
          backuptext.value = JSON.stringify(result, undefined, 2);
        })
        .catch((error) => {
          console.error(error);
        });
    };
  };

  signButton.onclick = async () => {
    signId(JSON.parse(cleartext.value))
      .then((result) => {
        signedtext.value = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  verifyButton.onclick = async () => {
    verifyId(JSON.parse(signedtext.value))
      .then((result) => {
        verifiedtext.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  addCreatedButton.onclick = async () => {
    addState(JSON.parse(identitycreated.textContent as string))
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  addUpdatedButton.onclick = async () => {
    addState(JSON.parse(identityUpdated.textContent as string))
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  createButton.onclick = async () => {
    createIdentity(JSON.parse(addtext.value))
      .then((result) => {
        identitycreated.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  getButton.onclick = async () => {
    getSubject(JSON.parse(gettext.value))
      .then((result) => {
        identitystate.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  verifySubjectButton.onclick = async () => {
    verifySubject(JSON.parse(identitystate.textContent as string))
      .then((result) => {
        identityVerification.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  getCredentialsButton.onclick = async () => {
    const issuers = await getIssuers(issuerContractAddress);
    // console.dir(issuers);
    getCredentials(nttContractAddress, useraddress.value)
      .then((result) => {
        nttCredentials.textContent = JSON.stringify(
          credentials2identity(
            result,
            parseInt(networkDiv.innerText, 10),
            nttContractAddress,
            issuers,
          ),
          undefined,
          2,
        );
      })
      .catch((error) => {
        console.error(error);
      });
  };

  saveCredentialsButton.onclick = async () => {
    const credentials = JSON.parse(nttCredentials.textContent as string);
    const identity = JSON.parse(identitycreated.textContent as string);
    identityUpdated.textContent = JSON.stringify(
      saveCredentials(credentials, identity),
      undefined,
      2,
    );
  };

  getIssuersButton.onclick = async () => {
    // console.log(`contract: ${nttContractAddress}, owner: ${useraddress.value}`);
    getIssuers(issuerContractAddress)
      .then((result) => {
        nttIssuers.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  getPublicKeyButton.onclick = async () => {
    // console.log(`contract: ${nttContractAddress}, owner: ${useraddress.value}`);
    getEncryptionPublicKey()
      .then((result) => {
        issuerPublicKey.value = result as string;
      })
      .catch((error) => {
        console.error(error);
      });
  };

  newIssuerButton.onclick = async () => {
    // console.log(`contract: ${nttContractAddress}, owner: ${useraddress.value}`);
    newIssuer(
      issuerContractAddress,
      issuerAddress.value,
      issuerName.value,
      issuerPublicKey.value,
      JSON.parse(issuerSubjects.value),
    )
      .then((result) => {
        issuerData.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  signSubjectsButton.onclick = async () => {
    signSubject(JSON.parse(holderSubjects.value))
      .then((result) => {
        issuerSignedSubjects.value = JSON.stringify(result);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  mintButton.onclick = async () => {
    // console.log(`contract: ${nttContractAddress}, holder: ${holderAddress.value}, subjects: ${JSON.parse(issuerSignedSubjects.value)}`);
    mint(
      nttContractAddress,
      holderAddress.value,
      JSON.parse(issuerSignedSubjects.value),
    )
      .then((result) => {
        mintData.textContent = JSON.stringify(result, undefined, 2);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  issuerPatchButton.onclick = async () => {
    console.log(await issuerApi.patch(sendIssuerData.value));
  };

  issuerGetButton.onclick = async () => {
    try {
      getIssuerData.textContent = JSON.stringify(
        JSON.parse((await issuerApi.get()) as string),
        undefined,
        2,
      );
    } catch (error) {
      console.error(error);
    }
  };

  userPatchButton.onclick = async () => {
    console.log(await userApi.patch(sendUserData.value));
  };

  userGetButton.onclick = async () => {
    try {
      getUserData.textContent = JSON.stringify(
        JSON.parse((await userApi.get()) as string),
        undefined,
        2,
      );
    } catch (error) {
      console.error(error);
    }
  };

  const updateButtons = async () => {
    if (await isConnected()) {
      onboardButton.innerText = 'Connected';
      onboardButton.disabled = true;
      await handleNetworkAndChainId();
      if (onboarding) {
        onboarding.stopOnboarding();
      }
      initializeAccountButtons();
      await handleAccount();
    } else if (isInstalled() && (await isFlask())) {
      onboardButton.innerText = 'Connect';
      onboardButton.onclick = onClickConnect;
      onboardButton.disabled = false;
      switchEthereumChain.disabled = true;
      snapEnable.disabled = true;
      accounts = [];
      await handleNetworkAndChainId();
    } else {
      onboardButton.innerText = 'Install MetaMask Flask';
      onboardButton.onclick = onClickInstall;
      onboardButton.disabled = false;
      switchEthereumChain.disabled = true;
      snapEnable.disabled = true;
    }

    // accountsDiv.innerHTML = accounts[0] || 'Not able to get accounts';
    isInstalledInput.checked = isInstalled();
    isFlaskInput.checked = await isFlask();
    isUnlockedInput.checked = await isUnlocked();
    isSnapInstalledInput.checked = await isSnapInstalled();
  };

  async function handleAccount() {
    wallet_getPermissions()
      .then((permissionsArray) => {
        permissionsResult.innerHTML =
          getPermissionsDisplayString(permissionsArray);

        console.log(
          `Snap ${snapOrigin} permited: ${isSnapPermited(
            permissionsArray,
            snapOrigin,
          )}`,
        );
      })
      .catch((error) => {
        console.error(error);
      });

    eth_accounts()
      .then(async (_accounts) => {
        accounts = _accounts;
        accountsDiv.innerHTML = accounts.join(', ');
      })
      .catch((error) => {
        console.error(error);
      });

    wallet_getSnaps()
      .then((result: WalletGetSnapsResult) => {
        // console.log(`GetSnaps Result:${JSON.stringify(result)}`);
        snapIdDiv.innerHTML = '';
        snapVersionDiv.innerHTML = '';
        snapEnable.disabled = false;
        isSnapInstalledInput.checked = false;
        if (Object.keys(result).length > 0) {
          if (
            result[snapOrigin].error &&
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore: Object is possibly 'null'.
            result[snapOrigin].error.code === -32603
          ) {
            console.log(JSON.stringify(result));
            // wallet_installSnaps(snapOrigin)
            // .then((install_result: WalletInstallSnapsResult) => {
            //   console.log(JSON.stringify(install_result));
            // })
            // .catch((error) => {
            //   console.error(error);
            // });
          } else {
            snapIdDiv.innerHTML = result[snapOrigin].id;
            snapVersionDiv.innerHTML = result[snapOrigin].version;
            snapEnable.disabled = Boolean(
              Object.values(result).find(
                (snap) =>
                  snap.id === snapOrigin && snap.version === snapVersion,
              ),
            );
            isSnapInstalledInput.checked = snapEnable.disabled;
          }
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  async function handleNetworkAndChainId() {
    eth_chainId()
      .then((chainId) => {
        chainIdDiv.innerHTML = chainId;
      })
      .catch((error) => {
        console.error(error);
      });

    net_version()
      .then((networkId) => {
        networkDiv.innerHTML = networkId;
        if (networkId === '80001') {
          switchEthereumChain.disabled = true;
        } else {
          switchEthereumChain.disabled = false;
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  onChainChanged(async () => {
    console.log('Saw Chain changed event');
    await handleNetworkAndChainId();
    window.location.reload();
  });

  onAccountsChanged(async () => {
    console.log('Saw Account changed event');
    await updateButtons();
  });

  onConnect(async () => {
    console.log('Saw connect event');
    await updateButtons();
  });

  await updateButtons();
};

window.addEventListener('load', main);

function getPermissionsDisplayString(permissionsArray: Web3WalletPermission[]) {
  if (permissionsArray.length === 0) {
    return 'No permissions found.';
  }
  // console.log(`permission result: ${JSON.stringify(permissionsArray)}`);
  const permissionNames = permissionsArray.map((perm) => perm.parentCapability);
  // return permissionNames
  //   .reduce((acc, name) => `${acc}${name}, `, '')
  //   .replace(/, $/u, '');
  return permissionNames.join(', ');
}

function isSnapPermited(
  permissionsArray: Web3WalletPermission[],
  snapOriginName: string,
) {
  if (permissionsArray.length === 0) {
    return false;
  }
  // console.log(`permission result: ${JSON.stringify(permissionsArray)}`);
  const permissionNames = permissionsArray.map((perm) => perm.parentCapability);
  return permissionNames.includes(`wallet_snap_${snapOriginName}`);
}
