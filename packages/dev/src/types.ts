/* eslint-disable @typescript-eslint/consistent-type-definitions */

export interface WalletEnableParam {
  wallet_snap: {
    [snapId: string]: {
      version?: string;
    };
  };
  [permissionName: string]: unknown;
}

interface Web3WalletPermission {
  parentCapability: string;
  date?: number;
}

export interface WalletEnableResult {
  accounts: string[];
  permissions: Web3WalletPermission[];
  snaps: WalletInstallSnapsResult;
  errors?: Error[];
}

interface InstallSnapsParam {
  [snapId: string]: {
    version?: string;
  };
}

export interface WalletInstallSnapsResult {
  [snapId: string]:
    | WalletGetSnapsResult[string]
    | {
        error: Error;
      };
}

interface RequestedPermissions {
  // eslint-disable-next-line @typescript-eslint/ban-types
  [methodName: string]: {};
}

type GetSnapsResponse = {
  [k: string]: {
    permissionName?: string;
    id?: string;
    version?: string;
    initialPermissions?: { [k: string]: unknown };
  };
};

export interface WalletGetSnapsResult {
  [snapId: string]: {
    id: string;
    initialPermissions?: { [k: string]: unknown };
    permissionName?: string;
    version: string;
    error?: {
      code: number;
      message: string;
      data: { [k: string]: unknown };
    };
  };
}

interface SnapRequest {
  method: string;
  params?: unknown[] | Record<string, unknown>;
  id?: string | number;
  jsonrpc?: '2.0';
}

interface SnapConfirmParam {
  prompt: string;
  description?: string;
  textAreaContent?: string;
}

interface BIP44CoinTypeNode {
  readonly coin_type: number;
  readonly depth: 2;
  readonly privateKey: string;
  readonly publicKey: string;
  readonly chainCode: string;
  readonly path: string;
}
