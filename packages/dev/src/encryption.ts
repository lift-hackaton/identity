// Based: https://betterprogramming.pub/exchanging-encrypted-data-on-blockchain-using-metamask-a2e65a9a896c

import {
  encrypt,
  encryptSafely,
  decrypt,
  decryptSafely,
  getEncryptionPublicKey,
  EthEncryptedData,
  recoverPersonalSignature,
  extractPublicKey,
} from '@metamask/eth-sig-util';

// Key is returned as base64
export const toBufferbase64 = (keyB64: string): Buffer => {
  return Buffer.from(keyB64, 'base64');
};

export const personalRecover = (message: string, sign: string): string => {
  const msg = `0x${Buffer.from(message, 'utf8').toString('hex')}`;
  return recoverPersonalSignature({
    data: msg,
    signature: sign,
  });
};

export const personalPublicKey = (message: string, sign: string): string => {
  const msg = `0x${Buffer.from(message, 'utf8').toString('hex')}`;
  const key = extractPublicKey({
    data: msg,
    signature: sign,
  });
  return Buffer.from(key.slice(2), 'hex').toString('base64');
};

export const encodeEncrypt = (
  _publicKey: string,
  _data: string,
): EthEncryptedData => {
  return encrypt({
    publicKey: _publicKey,
    data: _data,
    version: 'x25519-xsalsa20-poly1305',
  });
};

export const singleNumberArray = (enc: EthEncryptedData): number[] => {
  // concatenate them into single Buffer
  const buf = Buffer.concat([
    Buffer.from(enc.ephemPublicKey, 'base64'),
    Buffer.from(enc.nonce, 'base64'),
    Buffer.from(enc.ciphertext, 'base64'),
  ]);
  // In smart contract we are using `bytes[112]` variable (fixed size byte array)
  // you might need to use `bytes` type for dynamic sized array
  // We are also using ethers.js which requires type `number[]` when passing data
  // for argument of type `bytes` to the smart contract function
  // Next line just converts the buffer to `number[]` required by contract function
  return buf.toJSON().data;
};

export const singleBuffer = (enc: EthEncryptedData): Buffer => {
  // concatenate them into single Buffer
  const buf = Buffer.concat([
    Buffer.from(enc.ephemPublicKey, 'base64'),
    Buffer.from(enc.nonce, 'base64'),
    Buffer.from(enc.ciphertext, 'base64'),
  ]);
  // Return just the Buffer to make the function directly compatible with decryptData function
  return buf;
};

export const fromSingleBuffer = (data: Buffer): string => {
  // Reconstructing the original object outputed by encryption
  const structuredData = {
    version: 'x25519-xsalsa20-poly1305',
    ephemPublicKey: data.slice(0, 32).toString('base64'),
    nonce: data.slice(32, 56).toString('base64'),
    ciphertext: data.slice(56).toString('base64'),
  };
  // Convert data to hex string required by MetaMask
  return `0x${Buffer.from(JSON.stringify(structuredData), 'utf8').toString(
    'hex',
  )}`;
};
