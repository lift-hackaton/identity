/* eslint-disable camelcase */
import EventEmitter from 'events';
import { BigNumber, ethers } from 'ethers';
import { web3_clientVersion, selectedAddress, net_version } from './metamask';
import {
  snapOrigin,
  snapVersion,
  nttContractAddress,
  issuerContractAddress,
} from './config';
import {
  WalletGetSnapsResult,
  WalletEnableResult,
  WalletInstallSnapsResult,
} from './types';

import { nttAbi } from './IdentityNTT';
import { issuerAbi } from './IssuersRegistry';

const { ethereum } = window as unknown as {
  ethereum: {
    isMetaMask: boolean;
    isConnected: () => boolean;
    selectedAddress: string;
    request: <T>(arg: {
      method: string;
      params?: any[] | Record<string, any>;
    }) => Promise<T>;
    _metamask: {
      isUnlocked: () => Promise<boolean>;
    };
  } & EventEmitter;
};

// force EtherscanProvider even if no metamask just to not show errors when metamask is not installed
const provider = !ethereum
  ? (new ethers.providers.EtherscanProvider() as unknown as ethers.providers.Web3Provider)
  : new ethers.providers.Web3Provider(ethereum, 'any');

export const isFlask = async () => {
  try {
    const clientVersion = await web3_clientVersion();
    const isFlaskDetected = clientVersion.includes('flask');
    return Boolean(ethereum && isFlaskDetected);
  } catch {
    return false;
  }
};

export const wallet_enable = async (
  snapId: string = snapOrigin,
  params: Record<'version' | string, unknown> = {},
): Promise<WalletEnableResult> => {
  return ethereum.request({
    method: 'wallet_enable',
    params: [
      {
        wallet_snap: {
          [snapId]: {
            ...params,
          },
        },
      },
    ],
  });
};

export const wallet_getSnaps = async (): Promise<WalletGetSnapsResult> => {
  /*   console.log(
    `extra getSnaps result: ${JSON.stringify(
      await ethereum.request({ method: 'wallet_getSnaps' }),
    )}`,
  ); */

  return (await ethereum.request({
    method: 'wallet_getSnaps',
  })) as unknown as WalletGetSnapsResult;
};

export const isSnapInstalled = async (): Promise<boolean> => {
  try {
    if (!ethereum) {
      return false;
    }
    const snaps = await wallet_getSnaps();
    return Boolean(
      Object.values(snaps).find(
        (snap) => snap.id === snapOrigin && snap.version === snapVersion,
      ),
    );
  } catch (e) {
    console.log('Failed to obtain installed snaps', e);
    return false;
  }
};

export const wallet_installSnaps = async (
  snapId: string = snapOrigin,
  params: Record<'version' | string, unknown> = {},
): Promise<WalletInstallSnapsResult> => {
  return ethereum.request({
    method: 'wallet_installSnaps',
    params: [
      {
        [snapId]: {
          ...params,
        },
      },
    ],
  });
};

export const sendHello = async () => {
  const result = await ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'hello',
      },
    ],
  });
  console.log(`Ação do Usuário: ${result}`);
};

export const setState = async (identity: Record<string, unknown>[]) => {
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'setState',
        params: identity,
      },
    ],
  });
};

export const signId = async (subject: Record<string, unknown>) => {
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'signId',
        params: subject,
      },
    ],
  });
};

export const signSubject = async (subject: Record<string, unknown>) => {
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'signSubject',
        params: subject,
      },
    ],
  });
};

export const getSubject = async (subjects: string[]) => {
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'getSubjectData',
        params: subjects,
      },
    ],
  });
};

export const verifyId = async (identity: Record<string, unknown>[]) => {
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'verifyId',
        params: identity,
      },
    ],
  });
};

export const getState = async () => {
  return await ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'getState',
      },
    ],
  });
};

export const getEncryptionPublicKey = async (): Promise<string> => {
  return await ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'getEncryptionPublicKey',
      },
    ],
  });
};

export const getCredentials = async (
  contractAddress: string,
  userAddress: string,
) => {
  const user = ethereum.selectedAddress;
  const nttContract = new ethers.Contract(
    contractAddress,
    nttAbi,
    provider.getSigner(user),
  );
  // console.log(`nome contract: ${await nttContract.name()}`);
  return await nttContract.getCredentials(userAddress, {
    from: user,
  });

  /*
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'getCredentials',
        params: { contractAddress, userAddress },
      },
    ],
  });
   */
};

export const mint = async (
  contractAddress: string,
  userAddress: string,
  signedSubjects: Record<string, unknown>,
) => {
  const user = ethereum.selectedAddress;
  const nttContract = new ethers.Contract(
    contractAddress,
    nttAbi,
    provider.getSigner(user),
  );
  // console.log(`nome contract: ${await nttContract.name()}`);
  return await nttContract.mint(userAddress, JSON.stringify(signedSubjects), {
    from: user,
  });
};

export const getIssuers = async (contractAddress: string) => {
  const user = ethereum.selectedAddress;
  const issuerContract = new ethers.Contract(
    contractAddress,
    issuerAbi,
    provider.getSigner(user),
  );
  return await issuerContract.getIssuers({
    from: user,
  });
};

export const getIssuer = async (
  contractAddress: string,
  issuer: string,
): Promise<[string, string, string, string[], BigNumber]> => {
  const user = ethereum.selectedAddress;
  const issuerContract = new ethers.Contract(
    contractAddress,
    issuerAbi,
    provider.getSigner(user),
  );
  return await issuerContract.getIssuer(issuer, {
    from: user,
  });
};

export const newIssuer = async (
  contractAddress: string,
  address: string,
  name: string,
  publicKey: string,
  issuerSubjects: string[],
) => {
  const user = ethereum.selectedAddress;
  const issuerContract = new ethers.Contract(
    contractAddress,
    issuerAbi,
    provider.getSigner(user),
  );
  return await issuerContract.newIssuer(
    address,
    name,
    publicKey,
    issuerSubjects,
    {
      from: user,
    },
  );
};

type IdentityType = {
  version: number;
  subject: string;
  encrypted?: boolean;
  data?: string;
  user?: string;
  issuer?: string;
  issuerName?: string;
  networkId?: number;
  contract?: string;
  signature?: string;
  verify?: boolean;
};

export const addState = async (identity: IdentityType[]) => {
  return ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      snapOrigin,
      {
        method: 'addState',
        params: identity,
      },
    ],
  });
};

export const validIssuer = (
  filterissuer: string,
  subject: string,
  issuers?: [string, string, string, string[], BigNumber][],
): boolean => {
  return issuers
    ? issuers.findIndex(
        ([issuer, name, publicKey, subjects, dateTimeSuspension]) => {
          return (
            issuer?.toLowerCase() === filterissuer?.toLowerCase() &&
            subjects.includes(subject) &&
            dateTimeSuspension.toString() === '0'
          );
        },
      ) !== -1
    : true;
};

export const issuerName = (
  filterissuer: string,
  subject: string,
  issuers?: [string, string, string, string[], BigNumber][],
): string => {
  const validIssuers = issuers
    ? issuers.filter(
        ([issuer, name, publicKey, subjects, dateTimeSuspension]) => {
          return (
            issuer?.toLowerCase() === filterissuer?.toLowerCase() &&
            subjects.includes(subject) &&
            dateTimeSuspension.toString() === '0'
          );
        },
      )
    : ([] as [string, string, string, string[], BigNumber][]);
  return issuers
    ? validIssuers.reduce(
        (
          previousValue,
          [issuer, name, publicKey, subjects, dateTimeSuspension],
        ) => {
          return name;
        },
        '',
      )
    : '';
};

export const credentials2identity = (
  credential: [string, string, string, boolean][],
  networkId: number,
  contract: string,
  issuers?: [string, string, string, string[], BigNumber][],
  version = 1,
): IdentityType[] => {
  return credential.reduce(
    (previousCredential, [issuer, owner, data, valid]) => {
      return valid
        ? Object.entries(JSON.parse(data)).reduce(
            (previousValue, [key, value]) => {
              const identity = {} as IdentityType;
              identity.version = version;
              identity.subject = key;
              identity.user = owner;
              identity.issuer = issuer;
              if (issuers) {
                identity.issuerName = issuerName(issuer, key, issuers);
              }
              identity.networkId = networkId;
              identity.contract = contract;
              identity.signature = value as string;
              if (validIssuer(issuer, key, issuers)) {
                previousValue.push(identity);
              } else {
                console.log('No valid issuer subject');
              }
              return previousValue;
            },
            previousCredential as IdentityType[],
          )
        : previousCredential;
    },
    [] as IdentityType[],
  );
};

export const subject2Identity = (
  subject: Record<string, unknown>,
  version = 1,
) => {
  return Object.entries(subject).reduce((previousValue, [key, value]) => {
    const identity = {} as IdentityType;
    identity.version = version;
    identity.subject = key;
    identity.encrypted = false;
    identity.data = value as string;
    previousValue.push(identity);
    return previousValue;
  }, [] as IdentityType[]);
};

export const createIdentity = async (
  request: {
    subject: Record<string, unknown>;
    attestation: {
      issuer: string;
      issuerName?: string;
      networkId?: number;
      contract: string;
    }[];
  },
  user?: string,
  networkId?: number,
  version = 1,
) => {
  const defaultUser = user || (await selectedAddress());
  const defaultNetwork = networkId || parseInt(await net_version(), 10);
  if (request.attestation && request.attestation.length > 0) {
    return Object.entries(request.subject).reduce(
      (previousValue, [key, value]) => {
        request.attestation.forEach((currentValue) => {
          const identity = {} as IdentityType;
          identity.version = version;
          identity.subject = key;
          identity.encrypted = false;
          identity.data = value as string;
          identity.user = defaultUser;

          if (currentValue.contract) {
            if (currentValue.networkId) {
              identity.networkId = currentValue.networkId;
            } else {
              identity.networkId = defaultNetwork;
            }
            identity.contract = currentValue.contract;
            if (currentValue.issuer) {
              identity.issuer = currentValue.issuer;
            }

            if (currentValue.issuerName) {
              identity.issuerName = currentValue.issuerName;
            }
          }

          previousValue.push(identity);
        });
        return previousValue;
      },
      [] as IdentityType[],
    );
  }
  return subject2Identity(request.subject, version);
};

export const saveCredentials = (
  credential: IdentityType[],
  identity: IdentityType[],
) => {
  const verifiedIdentity = identity.map((currentIdentity) => {
    if (
      currentIdentity.issuer &&
      currentIdentity.networkId &&
      currentIdentity.contract
    ) {
      // testar se esta com o mesmo registro
      const found = credential.find((currentCredential) => {
        return (
          currentIdentity.version === currentCredential.version &&
          currentIdentity.subject === currentCredential.subject &&
          currentIdentity.user?.toLowerCase() ===
            currentCredential.user?.toLowerCase() &&
          currentIdentity.issuer?.toLowerCase() ===
            currentCredential.issuer?.toLowerCase() &&
          currentIdentity.networkId === currentCredential.networkId &&
          currentIdentity.contract?.toLowerCase() ===
            currentCredential.contract?.toLowerCase()
        );
      });
      if (found) {
        currentIdentity.signature = found.signature;
        if (found.issuerName) {
          currentIdentity.issuerName = found.issuerName;
        } else {
          delete currentIdentity.issuerName;
        }
        delete currentIdentity.verify;
      } else {
        // Testar se esta no mesmo contrato e user
        const sameContract = credential.some((currentCredential) => {
          return (
            currentIdentity.version === currentCredential.version &&
            currentIdentity.user?.toLowerCase() ===
              currentCredential.user?.toLowerCase() &&
            currentIdentity.networkId === currentCredential.networkId &&
            currentIdentity.contract?.toLowerCase() ===
              currentCredential.contract?.toLowerCase()
          );
        });
        // mesmo contrato e sem registro (= perdeu validade ou falso)
        if (sameContract) {
          currentIdentity.verify = false;
        }
      }
    } else {
      const found = credential.find((currentCredential) => {
        return (
          currentIdentity.version === currentCredential.version &&
          currentIdentity.subject === currentCredential.subject &&
          currentIdentity.user?.toLowerCase() ===
            currentCredential.user?.toLowerCase() &&
          (!currentIdentity.issuer ||
            currentIdentity.issuer?.toLowerCase() ===
              currentCredential.issuer?.toLowerCase())
        );
      });
      if (found) {
        currentIdentity.issuer = found.issuer;
        currentIdentity.networkId = found.networkId;
        currentIdentity.contract = found.contract;
        currentIdentity.signature = found.signature;
        if (found.issuerName) {
          currentIdentity.issuerName = found.issuerName;
        } else {
          delete currentIdentity.issuerName;
        }
        delete currentIdentity.verify;
      }
    }
    return currentIdentity;
  });
  const missingCredentials = credential
    .filter((currentCredential) => {
      if (
        !(
          currentCredential.version &&
          currentCredential.subject &&
          currentCredential.user &&
          currentCredential.issuer &&
          currentCredential.networkId &&
          currentCredential.contract &&
          currentCredential.signature &&
          (!currentCredential.verify || currentCredential.verify === true)
        )
      ) {
        return false;
      }
      const found = identity.find((currentIdentity) => {
        return (
          currentIdentity.version === currentCredential.version &&
          currentIdentity.subject === currentCredential.subject &&
          currentIdentity.user?.toLowerCase() ===
            currentCredential.user?.toLowerCase() &&
          currentIdentity.issuer?.toLowerCase() ===
            currentCredential.issuer?.toLowerCase() &&
          currentIdentity.networkId === currentCredential.networkId &&
          currentIdentity.contract?.toLowerCase() ===
            currentCredential.contract?.toLowerCase()
        );
      });
      const hasInfo = identity.find((currentIdentity) => {
        return (
          currentIdentity.version === currentCredential.version &&
          currentIdentity.subject === currentCredential.subject &&
          currentIdentity.user?.toLowerCase() ===
            currentCredential.user?.toLowerCase()
        );
      });
      return !(found || !hasInfo);
    })
    .map((currentCredential) => {
      const found = identity.find((currentIdentity) => {
        return (
          currentIdentity.version === currentCredential.version &&
          currentIdentity.subject === currentCredential.subject &&
          currentIdentity.user?.toLowerCase() ===
            currentCredential.user?.toLowerCase()
        );
      });
      if (found) {
        currentCredential.encrypted = found.encrypted;
        currentCredential.data = found.data;
        delete currentCredential.verify;
      }
      return currentCredential;
    });
  return verifiedIdentity
    .concat(missingCredentials) // Apagar todos os itens marcados com verify falso
    .filter((currentIdentity) => {
      return 'verify' in currentIdentity ? currentIdentity.verify : true;
    });
};

export const verifyMessageAddress = async (msg: string, sign: string) => {
  return await ethers.utils.verifyMessage(msg, sign);
};

export const verifyMessage = async (
  msg?: string,
  sign?: string,
  address?: string,
) => {
  if (msg && sign && address) {
    return (
      (await verifyMessageAddress(msg as string, sign as string)) === address
    );
  }
  return false;
};

export const verifySubject = async (identity: IdentityType[]) => {
  return await Promise.all(
    identity.map(async (currentValue) => {
      const verification = {} as IdentityType;
      // eslint-disable-next-line no-negated-condition
      if (!currentValue.encrypted) {
        verification.verify = await verifyMessage(
          currentValue.data,
          currentValue.signature,
          currentValue.issuer,
        );
      } else {
        // not yet implemented
      }
      // console.log(`currentValue is ${JSON.stringify(currentValue)}`);
      return Object.assign({}, currentValue, verification);
    }),
  );
};
