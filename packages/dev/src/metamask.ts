/* eslint-disable camelcase */
// https://ethereum.org/en/developers/docs/apis/json-rpc/#json-rpc-methods
// https://docs.metamask.io/guide/rpc-api.html#ethereum-json-rpc-methods

import EventEmitter from 'events';

const { ethereum } = window as unknown as {
  ethereum: {
    isMetaMask: boolean;
    isConnected: () => boolean;
    selectedAddress: string;
    request: <T>(arg: {
      method: string;
      params?: any[] | Record<string, any>;
    }) => Promise<T>;
    _metamask: {
      isUnlocked: () => Promise<boolean>;
    };
  } & EventEmitter;
};

export type Web3WalletPermission = {
  // The name of the method corresponding to the permission
  parentCapability: string;

  // The date the permission was granted, in UNIX epoch time
  date?: number;
};

export type RequestedPermissions = {
  [methodName: string]: Record<string, unknown>; // an empty object, for future extensibility
};

export const eth_decrypt = async (message: string, accountId?: string) => {
  return ethereum.request<string>({
    method: 'eth_decrypt',
    params: [message, accountId],
  });
};

export const eth_requestAccounts = async () => {
  return ethereum.request<string[]>({
    method: 'eth_requestAccounts',
  });
};

export const eth_getEncryptionPublicKey = async (accountId: string) => {
  return ethereum.request<string>({
    method: 'eth_getEncryptionPublicKey',
    params: [accountId],
  });
};

export const web3_clientVersion = async () => {
  return ethereum.request<string>({
    method: 'web3_clientVersion',
  });
};

export const wallet_getPermissions = async () => {
  return ethereum.request<Web3WalletPermission[]>({
    method: 'wallet_getPermissions',
  });
};

export const wallet_requestPermissions = async (
  permissions: RequestedPermissions,
) => {
  return ethereum.request<Web3WalletPermission[]>({
    method: 'wallet_requestPermissions',
    params: [permissions],
  });
};

export const wallet_requestAccountsPermissions = async () => {
  return ethereum.request<Web3WalletPermission[]>({
    method: 'wallet_requestPermissions',
    params: [{ eth_accounts: {} }],
  });
};

export type AddEthereumChainParameter = {
  chainId: string; // A 0x-prefixed hexadecimal string
  chainName: string;
  nativeCurrency: {
    name: string;
    symbol: string; // 2-6 characters long
    decimals: 18;
  };
  rpcUrls: string[];
  blockExplorerUrls?: string[];
  iconUrls?: string[]; // Currently ignored.
};

export const wallet_addEthereumChain = async (
  chainParameter: AddEthereumChainParameter,
) => {
  return ethereum.request<null>({
    method: 'wallet_addEthereumChain',
    params: [chainParameter],
  });
};

export type SwitchEthereumChainParameter = {
  chainId: string; // A 0x-prefixed hexadecimal string
};

export const wallet_switchEthereumChain = async (_chainId: string) => {
  return await ethereum.request<null>({
    method: 'wallet_switchEthereumChain',
    params: [{ chainId: _chainId } as SwitchEthereumChainParameter],
  });
};

export const wallet_registerOnboardingCompleted = async () => {
  return await ethereum.request<boolean>({
    method: 'wallet_registerOnboardingCompleted',
  });
};

export const eth_chainId = async () => {
  return ethereum.request<string>({ method: 'eth_chainId' });
};

export const net_version = async () => {
  return ethereum.request<string>({ method: 'net_version' });
};

export type WatchAssetParams = {
  type: 'ERC20'; // In the future, other standards will be supported
  options: {
    address: string; // The address of the token contract
    symbol: string; // A ticker symbol or shorthand, up to 5 characters
    decimals: number; // The number of token decimals
    image: string; // A string url of the token logo
  };
};

export const wallet_watchAsset = async (params: WatchAssetParams) => {
  return ethereum.request<boolean>({
    method: 'wallet_watchAsset',
    params,
  });
};

export const wallet_scanQRCode = async (regex?: string) => {
  return ethereum.request<string>({
    method: 'wallet_scanQRCode',
    params: [regex],
  });
};

export const personal_sign = async (
  msg: string,
  from: string,
  password: string,
) => {
  return ethereum.request<string>({
    method: 'personal_sign',
    params: [msg, from, password],
  });
};

export const personal_ecRecover = async (msg: string, sign: string) => {
  return ethereum.request<string>({
    method: 'personal_ecRecover',
    params: [msg, sign],
  });
};

export type MetamaskEvents =
  | 'accountsChanged'
  | 'chainChanged'
  | 'connect'
  | 'disconnect'
  | 'message';

export const listen = <T>(
  event: MetamaskEvents,
  handler: (args: T) => void,
) => {
  if (!ethereum) {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    return () => {};
  }
  console.log('Adding listener for event', event);
  ethereum.on(event, handler);
  return () => {
    console.log('Removing listener for event', event);
    ethereum.removeListener(event, handler);
  };
};

export const onAccountsChanged = (handler: (accounts: string[]) => void) => {
  return listen<string[]>('accountsChanged', handler);
};

export const onChainChanged = (handler: (chain: string) => void) => {
  return listen<string>('chainChanged', handler);
};

export type ConnectInfo = {
  chainId: string;
};

export const onConnect = (handler: (connectInfo: ConnectInfo) => void) => {
  return listen<ConnectInfo>('connect', handler);
};

export const onDisconnect = (handler: (error: ProviderRpcError) => void) => {
  return listen<ProviderRpcError>('disconnect', handler);
};

export type ProviderMessage = {
  type: string;
  data: unknown;
};

export const onMessage = (handler: (message: ProviderMessage) => void) => {
  return listen<ProviderMessage>('message', handler);
};

export type ProviderRpcError = {
  message: string;
  code: number;
  data?: unknown;
} & Error;

export const isEthereum = Boolean(ethereum);

export const isInstalled = () => {
  // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
  return ethereum && ethereum.isMetaMask;
};

export const selectedAddress = () => {
  return ethereum.selectedAddress;
};

export const eth_accounts = async () => {
  return ethereum ? ethereum.request<string[]>({ method: 'eth_accounts' }) : [];
};

export const isConnected = async () => {
  const accounts = await eth_accounts();
  // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
  return ethereum && ethereum.isConnected() && accounts && accounts.length > 0;
};

export const isUnlocked = async () => {
  return ethereum && (await ethereum._metamask.isUnlocked());
};

export const numberToHex = function (value: number) {
  return `0x${value.toString(16)}`;
};

export const stringToHex = function (value: string) {
  return `0x${Buffer.from(value, 'utf8').toString('hex')}`;
};

export const dataToHex = function (value: object) {
  return `0x${Buffer.from(JSON.stringify(value), 'utf8').toString('hex')}`;
};

export const connectAccounts = async () => {
  // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
  if (ethereum && ethereum.isConnected()) {
    const permissionsArray = await wallet_getPermissions();
    if (
      permissionsArray
        .map((perm) => perm.parentCapability)
        .includes('eth_accounts')
    ) {
      const accounts = await eth_accounts();
      if (accounts && accounts.length > 0) {
        return accounts;
      }
    }

    if (await ethereum._metamask.isUnlocked()) {
      const accounts = await eth_requestAccounts();
      if (accounts && accounts.length > 0) {
        return accounts;
      }
    }
  }
  throw new Error('Try unlock and authorize account to connect');
};
