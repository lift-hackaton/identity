// eslint-disable-next-line node/no-unpublished-require
const tailwindcss = require('tailwindcss');

module.exports = {
  plugins: ['postcss-preset-env', tailwindcss],
};
