---
title: Token de Identidade Digital
separator: <!--s-->
verticalSeparator: <!--v-->
theme: night
revealOptions:
  transition: "fade"
---

### Token de Identidade Digital

![](media/identity.png)

<!--s-->

### Token de Identidade Digital

##### _(Lift Hackaton)_

Ademar Arvati Filho<br />
Fabiano Rodrigo Alves Nascimento<br />
Pedro Sena Barbosa Holtz Yen<br />
<br />
_LIFT Learning – nov/2022_

<!--s-->

## Identidade Digital

Este projeto busca desenvolver um sistema de emissão e verificação segura de tokens equivalentes à uma identidade, utilizando blockchain e criptografia.

<!--v-->

## Identidade Digital

![](media/triangle.avif)

<!--v-->

## Identidade Digital

O usuário solicita sua identidade preenchendo um formulário no site da entidade com seus dados pessoais e carteira metamask com um plugin desenvolvido para a guarda desta identidade.

<!--v-->

## Identidade Digital

Estes dados serão recebidos e validados por um funcionário desta entidade que emitirá um token na blockchain somente com a assinatura das informações mantendo a privacidade dos dados do usuário (nenhum dado onchain salvo).

<!--v-->

## Identidade Digital

O usuário então receberá sua identidade na carteira informada no formulário e poderá fornecê-la digitalmente a organizações que desejem verificá-la.


<!--s-->

## Desenvolvimento

### Token

<!--v-->

### Token

Non Transferable Token - NTT

_Adaptado do EIP-4671_

[🔗 EIP-4671](https://eips.ethereum.org/EIPS/eip-4671)

<!--v-->

### Token

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=../../ntt/contracts/ERC4671.sol&lines=19-24) -->
<!-- The below code snippet is automatically added from ../../ntt/contracts/ERC4671.sol -->
```sol
    struct Token {
        address issuer;
        address owner;
        string data;
        bool valid;
    }
```
<!-- AUTO-GENERATED-CONTENT:END -->

<!--v-->

### Token
##### _( exemplo tokenid 7 )_

issuer:0x7cE66E61414A205ffE1eA7B8cc094d64b95C211F,

owner:0xC2689b42eACb9064dB139746AA3F9936b5a1f241,

data:{"cpf":"0x9bd...","nome":"0x70894.."}

valid:true

[🔗 Deploy no Mumbai](https://mumbai.polygonscan.com/address/0x55Fc029fcE6cd79d3DE4f9dA5727eE24F672066b#code)


<!--v-->

### Token

Contrato auxiliar para Controle Acesso

_Issuers_

Emissores Autorizados

[🔗 Deploy no Mumbai](https://mumbai.polygonscan.com/address/0x6F5534F37311fC16105c435a7776bA53a0a1dc57#code)

<!--v-->

### Issuer

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=../../ntt/contracts/IssuersRegistry.sol&lines=36-42) -->
<!-- The below code snippet is automatically added from ../../ntt/contracts/IssuersRegistry.sol -->
```sol
    struct Issuer {
        address issuer;
        string name;
        bytes publicKey;
        string[] subjects;
        uint256 dateTimeSuspension;
    }
```
<!-- AUTO-GENERATED-CONTENT:END -->

<!--s-->

## Desenvolvimento

### Snap

<!--v-->

### Snap

![](media/Snaps_Fox.svg)

[🔗 Flask Snaps](https://metamask.io/snaps/)

<!--v-->

### Snap

Permite a extensão de funcionalidades do Metamask

* Salvar Informações adicionais
* Interface para acessar dados
* Acessar par de chaves para outras funções

<!--v-->

### Snap

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=../../snap/src/index.js&lines=52-63) -->
<!-- The below code snippet is automatically added from ../../snap/src/index.js -->
```js
    case 'signSubject':
      subject = request.params;
      if (
        await prompt({
          prompt: 'Assinar Dados',
          description: `Assinar os seguintes dados de ${origin}`,
          textAreaContent: JSON.stringify(subject, undefined, 2),
        })
      ) {
        return await signSubject(subject);
      }
      return {};
```
<!-- AUTO-GENERATED-CONTENT:END -->

[🔗 npm identity-snap](https://www.npmjs.com/package/@arvati/identity-snap)

<!--s-->

## Desenvolvimento

### Frontend

<!--v-->

### Frontend

![](media/triangle_issuer.avif)

<!--v-->

### Cert-Sign

[🔗 Solicitar Identidade](https://lift-hackaton.gitlab.io/identity/cert-sign/user.html)

[🔗 Criar Credencial / Identidade](https://lift-hackaton.gitlab.io/identity/cert-sign/funcionario.html)

<!--v-->

### Frontend

![](media/triangle_verifier.avif)

<!--v-->

### Seguradora

[🔗 Usar Identidade](https://lift-hackaton.gitlab.io/identity/seguradora/user.html)

<!--v-->

### Frontend

![](media/triangle_final.avif)

<!--v-->

### Seguradora

[🔗 Verificar Identidade](https://lift-hackaton.gitlab.io/identity/seguradora/funcionario.html)


<!--s-->

## Projeto

Código fonte e documentação

[🔗 gitlab.com/lift-hackaton/identity](https://gitlab.com/lift-hackaton/identity)

<!--s-->

## Demonstração

[![video demonstração](media/video.png)](https://gitlab.com/lift-hackaton/identity/-/raw/master/packages/doc/slides/media/show.mp4)
