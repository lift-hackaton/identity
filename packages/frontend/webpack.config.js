/* eslint-disable require-unicode-regexp */
/* eslint-disable node/no-unpublished-require */
const path = require('path');
// eslint-disable-next-line no-unused-vars
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const { ProvidePlugin } = require('webpack');

const pages = [
  'cert-sign/user',
  'cert-sign/funcionario',
  'seguradora/user',
  'seguradora/funcionario',
];

module.exports = {
  devtool: 'eval-source-map',
  entry: pages.reduce((config, page) => {
    config[page] = [`./src/${page}.js`, './src/index.ts'];
    return config;
  }, {}),

  output: {
    clean: true,
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js',
  },
  mode: 'development',
  devServer: {
    historyApiFallback: true,
    static: path.resolve(__dirname, './dist'),
    open: true,
    compress: true,
    hot: true,
    port: 3000,
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      { test: /\.ts$/, use: 'ts-loader' },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    fallback: {
      crypto: require.resolve('crypto-browserify'),
      stream: require.resolve('stream-browserify'),
      assert: require.resolve('assert'),
      http: require.resolve('stream-http'),
      https: require.resolve('https-browserify'),
      os: require.resolve('os-browserify'),
      url: require.resolve('url'),
    },
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: './src/cert-sign/user.html',
      filename: 'cert-sign/user.html',
      chunks: ['cert-sign/user'],
    }),

    new HtmlWebpackPlugin({
      inject: true,
      template: './src/cert-sign/funcionario.html',
      filename: 'cert-sign/funcionario.html',
      chunks: ['cert-sign/funcionario'],
    }),

    new HtmlWebpackPlugin({
      inject: true,
      template: './src/seguradora/user.html',
      filename: 'seguradora/user.html',
      chunks: ['seguradora/user'],
    }),

    new HtmlWebpackPlugin({
      inject: true,
      filename: 'seguradora/funcionario.html',
      template: './src/seguradora/funcionario.html',
      chunks: ['seguradora/funcionario'],
    }),

    new ProvidePlugin({
      process: 'process/browser',
      Buffer: ['buffer', 'Buffer'],
    }),
  ],
};
