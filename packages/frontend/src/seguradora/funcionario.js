import {
  getIssuers,
  getCredentials,
  saveCredentials,
  credentials2identity,
  verifySubject,
 } from '../flask';
 import {
  net_version,
  eth_accounts,
} from '../metamask';
import {
  issuerContractAddress,
   nttContractAddress
   } from '../config';

import { userApi } from '../server';


const getButton = document.getElementById('buscarIdentidade');
const verifydButton = document.getElementById('verificarIdentidade');
verifydButton.disabled = true;
const nameInput = document.querySelector('input[name=nome]');
const cpfInput = document.querySelector('input[name=cpf]');
const carteiraInput = document.querySelector('input[name=carteira]');

const nomeassinado = document.getElementById('nomeassinado');
const cpfassinado = document.getElementById('cpfassinado');

let identidade = [];

getButton.onclick = async () => {
  identidade = JSON.parse(await userApi.get());
  nameInput.value = identidade.find(currentValue => {
    return currentValue.subject === 'nome'
  })?.data;
  cpfInput.value = identidade.find(currentValue => {
    return currentValue.subject === 'cpf'
  })?.data
  verifydButton.disabled = false;
}

verifydButton.onclick = async () => {
  const issuers = await getIssuers(issuerContractAddress);
  const user = await eth_accounts()
  const credenciais = await getCredentials(nttContractAddress, user[0])
  const networkId = await net_version()
    const credencial = await credentials2identity(
      credenciais,
      parseInt(networkId, 10),
      nttContractAddress,
      issuers,
    )
    const identidade_com_credencial = await saveCredentials(credencial, identidade);
    const verificado = await verifySubject(identidade_com_credencial)
    // console.dir(verificado);
    nomeassinado.innerText = verificado.find(currentValue => {
      return currentValue.subject === 'nome' && currentValue.verify
    })?.issuerName || 'erro de verificação';
    cpfassinado.innerText = verificado.find(currentValue => {
      return currentValue.subject === 'cpf' && currentValue.verify
    })?.issuerName || 'erro de verificação';
}

