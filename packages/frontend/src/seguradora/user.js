import { userApi } from '../server';
import {
  getSubject,
} from '../flask';

const fillButton = document.getElementById('preencherIdentidade');
const sendButton = document.getElementById('enviarIdentidade');
sendButton.disabled = true;
const nameInput = document.querySelector('input[name=nome]');
const cpfInput = document.querySelector('input[name=cpf]');
const carteiraInput = document.querySelector('input[name=carteira]');

let identidade = [];

fillButton.onclick = async () => {
  const get = ['cpf', 'nome']
  identidade = await getSubject(get);
  nameInput.value = identidade.find(currentValue => {
    return currentValue.subject === 'nome'
  })?.data;
  cpfInput.value = identidade.find(currentValue => {
    return currentValue.subject === 'cpf'
  })?.data
  sendButton.disabled = false;
};

sendButton.onclick = async () => {
  const result = await userApi.patch(JSON.stringify(identidade))
}
