import {signSubject, mint, getEncryptionPublicKey, getState } from '../flask';
import { nttContractAddress } from '../config';

import { issuerApi } from '../server';

const issuerGetButton = document.getElementById('getButton');
const issueCredencial = document.getElementById('issueCredencial');
const nameInput = document.querySelector('input[name=nome]');
const cpfInput = document.querySelector('input[name=cpf]');
const userInput = document.querySelector('input[name=user]');
const carteiraInput = document.querySelector('input[name=carteira]');

issueCredencial.disabled = true;
var userData;

issuerGetButton.onclick = async () => {
    userData = JSON.parse(await issuerApi.get());
    cpfInput.value = userData.cpf;
    nameInput.value = userData.nome;
    userInput.value = userData.user;
    issueCredencial.disabled = false;
};

issueCredencial.onclick = async () => {
    const holderAddress = userData.user;
    const subjects = {
        "cpf": userData.cpf,
        "nome": userData.nome,
    }
    const signedSubjects = await signSubject(subjects);
    const mintData = await mint(
        nttContractAddress,
        holderAddress,
        signedSubjects
    );
}




