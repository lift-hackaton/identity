import { issuerApi } from '../server';
import {
  nttContractAddress,
  issuerContractAddress,
} from '../config';
import {
  setState,
  addState,
  getState,
  createIdentity,
  getIssuers,
  getCredentials,
  credentials2identity,
  saveCredentials,
} from '../flask';
import {
  net_version,
  eth_accounts,
} from '../metamask';

const createButton = document.getElementById('solicitarIdentidade');
const getCredentialsButton = document.getElementById('buscarCredencial');
getCredentialsButton.disabled = true;
const nameInput = document.querySelector('input[name=nome]');
const cpfInput = document.querySelector('input[name=cpf]');

createButton.onclick = async () => {
  const user = await eth_accounts()
  const request = {
    subject : {
    cpf: cpfInput.value,
    nome: nameInput.value,
    },
    attestation : [{
      issuer: '0x7cE66E61414A205ffE1eA7B8cc094d64b95C211F',
      contract: nttContractAddress,
    }]
  };
  const identidade = await createIdentity(request);
  const result1 = await setState(identidade);
  // const result1 = await addState(identidade);
  request.subject.user = user[0];
  const result2 = await issuerApi.patch(JSON.stringify(request.subject));
  getCredentialsButton.disabled = false;
  };

  getCredentialsButton.onclick = async () => {
    const issuers = await getIssuers(issuerContractAddress);
    const user = await eth_accounts()
    const credenciais = await getCredentials(nttContractAddress, user[0])
    const networkId = await net_version()
    const identidade = await credentials2identity(
      credenciais,
      parseInt(networkId, 10),
      nttContractAddress,
      issuers,
    )
    const state = await getState();
    const identidade_com_credencial = await saveCredentials(identidade,state);
    const result1 = await setState(identidade_com_credencial);
  };
