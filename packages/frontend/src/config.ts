/**
 * The snap origin to use.
 * Will default to the local hosted snap if no value is provided in environment.
 */

export const defaultSnapOrigin =
  process.env.REACT_APP_SNAP_ORIGIN ?? `local:http://localhost:8080`;

const currentUrl = new URL(window.location.href);

export const snapOrigin =
  currentUrl.hostname === 'localhost'
    ? `local:${window.location.protocol}//${window.location.hostname}:8080`
    : `npm:@arvati/identity-snap`;

export const snapVersion = '1.0.0';

export const forwarderOrigin =
  currentUrl.hostname === 'localhost' ? 'http://localhost:4000' : undefined;

export const nttContractAddress = '0x55Fc029fcE6cd79d3DE4f9dA5727eE24F672066b';

export const issuerContractAddress =
  '0x6F5534F37311fC16105c435a7776bA53a0a1dc57';
