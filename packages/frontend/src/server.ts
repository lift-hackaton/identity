/* eslint-disable import/unambiguous */
function request(url: string, config: RequestInit) {
  return fetch(url, config)
    .then((response) => response.text())
    .then((data) => data)
    .catch((error) => {
      console.error('Failed retrieving information', error);
    });
}

const api = {
  get: (url: string) =>
    request(url, {
      method: 'GET',
      redirect: 'follow',
    }),
  delete: (url: string) =>
    request(url, {
      method: 'DELETE',
      redirect: 'follow',
    }),
  post: (url: string, data: string) =>
    request(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      redirect: 'follow',
      body: data,
    }),
  put: (url: string, data: string) =>
    request(url, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      redirect: 'follow',
      body: data,
    }),
  patch: (url: string, data: string) =>
    request(url, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      redirect: 'follow',
      body: data,
    }),
};

export const issuerApi = {
  get: async () => {
    const issuerDataArray = JSON.parse(
      (await api.get('https://lift.deta.dev/api/issuer/')) as string,
    );
    if (issuerDataArray.length > 0) {
      return JSON.stringify(issuerDataArray.pop());
    }
    return '{}';
  },
  patch: async (body: string) => {
    const issuerDataArray = [];
    issuerDataArray.push(JSON.parse(body));
    return await api.patch(
      'https://lift.deta.dev/api/issuer/',
      JSON.stringify(issuerDataArray),
    );
  },
};

export const userApi = {
  get: async () => await api.get('https://lift.deta.dev/api/user/'),
  patch: async (body: string) =>
    await api.patch('https://lift.deta.dev/api/user/', body),
};
