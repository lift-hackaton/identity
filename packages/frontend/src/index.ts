/* eslint-disable camelcase */
import FlaskOnboarding from './onboarding';
import {
  toBufferbase64,
  encodeEncrypt,
  singleNumberArray,
  singleBuffer,
  fromSingleBuffer,
  personalPublicKey,
} from './encryption';

import {
  isFlask,
  isSnapInstalled,
  wallet_getSnaps,
  wallet_enable,
  wallet_installSnaps,
  setState,
  getState,
  addState,
  getSubject,
  signId,
  signSubject,
  verifyId,
  getCredentials,
  getIssuers,
  newIssuer,
  getEncryptionPublicKey,
  mint,
} from './flask';

import {
  eth_chainId,
  net_version,
  eth_accounts,
  selectedAddress,
  eth_requestAccounts,
  connectAccounts,
  Web3WalletPermission,
  wallet_requestPermissions,
  wallet_getPermissions,
  wallet_switchEthereumChain,
  wallet_addEthereumChain,
  eth_getEncryptionPublicKey,
  eth_decrypt,
  personal_sign,
  personal_ecRecover,
  isConnected,
  isInstalled,
  isUnlocked,
  onConnect,
  onChainChanged,
  onAccountsChanged,
  numberToHex,
  stringToHex,
} from './metamask';

import './styles/index.css';

import {
  forwarderOrigin,
  snapOrigin,
  snapVersion,
  nttContractAddress,
  issuerContractAddress,
} from './config';

import {
  WalletGetSnapsResult,
  WalletEnableResult,
  WalletInstallSnapsResult,
} from './types';

const networkDiv = document.getElementById('network') as HTMLSpanElement;
const chainIdDiv = document.getElementById('chainId') as HTMLSpanElement;
const accountsDiv = document.getElementById('accounts') as HTMLSpanElement;
const permissionsResult = document.getElementById(
  'permissionsResult',
) as HTMLSpanElement;
const snapIdDiv = document.getElementById('snapid') as HTMLSpanElement;
const snapVersionDiv = document.getElementById(
  'snapversion',
) as HTMLSpanElement;
const isInstalledInput = document.getElementById(
  'isInstalled',
) as HTMLInputElement;
const isFlaskInput = document.getElementById('isFlask') as HTMLInputElement;
const isUnlockedInput = document.getElementById(
  'isUnlocked',
) as HTMLInputElement;
const isSnapInstalledInput = document.getElementById(
  'isSnapInstalled',
) as HTMLInputElement;

const onboardButton = document.getElementById(
  'connectButton',
) as HTMLButtonElement;
const switchEthereumChain = document.getElementById(
  'switchEthereumChain',
) as HTMLButtonElement;
const snapEnable = document.getElementById('snapEnable') as HTMLButtonElement;

const userAddressInput = document.querySelector(
  'input[name=carteira]',
) as HTMLInputElement;
const userNetworkInput = document.querySelector(
  'input[name=network]',
) as HTMLInputElement;

let accounts: string[] = [];

const main = async () => {
  const onboarding = new FlaskOnboarding({ forwarderOrigin });

  const onClickConnect = async () => {
    connectAccounts()
      .then((_accounts) => {
        accounts = _accounts;
      })
      .catch((error) => {
        if (error.code === 4001) {
          // EIP-1193 userRejectedRequest error
          console.log('Please connect to MetaMask.');
        } else {
          console.error(error);
        }
      });
  };

  const onClickInstall = () => {
    onboardButton.innerText = 'Onboarding in progress';
    onboardButton.disabled = true;
    onboarding.startOnboarding();
  };

  const initializeAccountButtons = () => {
    //
    switchEthereumChain.onclick = async () => {
      wallet_switchEthereumChain(numberToHex(80001))
        .then(async () => {
          await handleNetworkAndChainId();
        })
        .catch((error) => {
          if (error.code === 4902) {
            // This error code indicates that the chain has not been added to MetaMask.
            wallet_addEthereumChain({
              chainId: numberToHex(80001),
              rpcUrls: ['https://rpc-mumbai.maticvigil.com'],
              chainName: 'Mumbai Testnet',
              nativeCurrency: {
                name: 'tMATIC',
                symbol: 'tMATIC',
                decimals: 18,
              },
              blockExplorerUrls: ['https://mumbai.polygonscan.com/'],
            })
              .then(async () => {
                await handleNetworkAndChainId();
              })
              .catch((err) => {
                console.error(err);
              });
          } else {
            console.error(error);
          }
        });
    };

    snapEnable.onclick = async () => {
      console.log(`enabling: ${snapOrigin} , version: ${snapVersion}`);
      wallet_enable(snapOrigin, { version: snapVersion })
        .then(async (result: WalletEnableResult) => {
          // console.log(`Snap Enable result: ${JSON.stringify(result)}`);
          if (result.snaps[snapOrigin].error) {
            console.error(result.snaps[snapOrigin].error);
          } else {
            await handleAccount();
          }
        })
        .catch((error) => {
          if (error.code === 4001) {
            // EIP-1193 userRejectedRequest error
            console.log('Please connect to MetaMask Snap.');
          } else {
            console.error(error);
          }
        });
    };
  };

  const updateButtons = async () => {
    if (await isConnected()) {
      onboardButton.innerText = 'Connected';
      onboardButton.disabled = true;
      await handleNetworkAndChainId();
      if (onboarding) {
        onboarding.stopOnboarding();
      }
      initializeAccountButtons();
      await handleAccount();
    } else if (isInstalled() && (await isFlask())) {
      onboardButton.innerText = 'Connect';
      onboardButton.onclick = onClickConnect;
      onboardButton.disabled = false;
      switchEthereumChain.disabled = true;
      snapEnable.disabled = true;
      accounts = [];
      await handleNetworkAndChainId();
    } else {
      onboardButton.innerText = 'Install MetaMask Flask';
      onboardButton.onclick = onClickInstall;
      onboardButton.disabled = false;
      switchEthereumChain.disabled = true;
      snapEnable.disabled = true;
    }

    // accountsDiv.innerHTML = accounts[0] || 'Not able to get accounts';
    isInstalledInput.checked = isInstalled();
    isFlaskInput.checked = await isFlask();
    isUnlockedInput.checked = await isUnlocked();
    isSnapInstalledInput.checked = await isSnapInstalled();
  };

  async function handleAccount() {
    wallet_getPermissions()
      .then((permissionsArray) => {
        permissionsResult.innerHTML =
          getPermissionsDisplayString(permissionsArray);

        console.log(
          `Snap ${snapOrigin} permited: ${isSnapPermited(
            permissionsArray,
            snapOrigin,
          )}`,
        );
      })
      .catch((error) => {
        console.error(error);
      });

    eth_accounts()
      .then(async (_accounts) => {
        accounts = _accounts;
        accountsDiv.innerHTML = accounts.join(', ');
        userAddressInput.value = accounts.join(', ');
      })
      .catch((error) => {
        console.error(error);
      });

    wallet_getSnaps()
      .then((result: WalletGetSnapsResult) => {
        // console.log(`GetSnaps Result:${JSON.stringify(result)}`);
        snapIdDiv.innerHTML = '';
        snapVersionDiv.innerHTML = '';
        snapEnable.disabled = false;
        isSnapInstalledInput.checked = false;
        if (Object.keys(result).length > 0) {
          if (
            result[snapOrigin].error &&
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore: Object is possibly 'null'.
            result[snapOrigin].error.code === -32603
          ) {
            console.log(JSON.stringify(result));
            // wallet_installSnaps(snapOrigin)
            // .then((install_result: WalletInstallSnapsResult) => {
            //   console.log(JSON.stringify(install_result));
            // })
            // .catch((error) => {
            //   console.error(error);
            // });
          } else {
            snapIdDiv.innerHTML = result[snapOrigin].id;
            snapVersionDiv.innerHTML = result[snapOrigin].version;
            snapEnable.disabled = Boolean(
              Object.values(result).find(
                (snap) =>
                  snap.id === snapOrigin && snap.version === snapVersion,
              ),
            );
            isSnapInstalledInput.checked = snapEnable.disabled;
          }
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  async function handleNetworkAndChainId() {
    eth_chainId()
      .then((chainId) => {
        chainIdDiv.innerHTML = chainId;
      })
      .catch((error) => {
        console.error(error);
      });

    net_version()
      .then((networkId) => {
        networkDiv.innerHTML = networkId;
        userNetworkInput.value = networkId;
        if (networkId === '80001') {
          switchEthereumChain.disabled = true;
        } else {
          switchEthereumChain.disabled = false;
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  onChainChanged(async () => {
    console.log('Saw Chain changed event');
    await handleNetworkAndChainId();
    window.location.reload();
  });

  onAccountsChanged(async () => {
    console.log('Saw Account changed event');
    await updateButtons();
  });

  onConnect(async () => {
    console.log('Saw connect event');
    await updateButtons();
  });

  await updateButtons();
};

window.addEventListener('load', main);

function getPermissionsDisplayString(permissionsArray: Web3WalletPermission[]) {
  if (permissionsArray.length === 0) {
    return 'No permissions found.';
  }
  // console.log(`permission result: ${JSON.stringify(permissionsArray)}`);
  const permissionNames = permissionsArray.map((perm) => perm.parentCapability);
  // return permissionNames
  //   .reduce((acc, name) => `${acc}${name}, `, '')
  //   .replace(/, $/u, '');
  return permissionNames.join(', ');
}

function isSnapPermited(
  permissionsArray: Web3WalletPermission[],
  snapOriginName: string,
) {
  if (permissionsArray.length === 0) {
    return false;
  }
  // console.log(`permission result: ${JSON.stringify(permissionsArray)}`);
  const permissionNames = permissionsArray.map((perm) => perm.parentCapability);
  return permissionNames.includes(`wallet_snap_${snapOriginName}`);
}
