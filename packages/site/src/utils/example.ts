export const id = [
  {
    version: 1,
    subject: 'cpf',
    encrypted: false,
    data: 'cpf.num.ero-xx',
    user: '0x9870...',
    issuer: '0x9870...',
    chainid: '8001',
    contract: '0x6259...',
    signature: 'aquelehash',
  },
  {
    version: 1,
    subject: 'nome',
    encrypted: false,
    data: 'meu nome completo',
    user: '0x9870...',
    issuer: '0x9870...',
    chainid: '8001',
    contract: '0x6259...',
    signature: 'aquelehashdonome',
  },
];
