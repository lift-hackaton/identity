import { defaultSnapOrigin } from '../config';

/**
 * Invoke the "hello" method from the example snap.
 */

export const sendHello = async () => {
  const result = await window.ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      defaultSnapOrigin,
      {
        method: 'hello',
      },
    ],
  });
  console.log(`Ação do Usuário: ${result}`);
};

export const setState = async (identity: Record<string, unknown>[]) => {
  const result = await window.ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      defaultSnapOrigin,
      {
        method: 'setState',
        params: identity,
      },
    ],
  });
  console.log(`Ação do Usuário: ${result}`);
};

export const getState = async () => {
  const result = await window.ethereum.request({
    method: 'wallet_invokeSnap',
    params: [
      defaultSnapOrigin,
      {
        method: 'getState',
      },
    ],
  });
  console.log(`State do Snap: ${JSON.stringify(result)}`);
};
