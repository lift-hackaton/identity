/* eslint-disable node/no-unpublished-import */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-case-declarations */
import { ethErrors } from 'eth-rpc-errors';
import { ethers } from 'ethers';
import {
  prompt,
  updateState,
  getState,
  addState,
  notify,
  selectedAddress,
  getPrivateKey,
  getEncryptionPublicKey,
  signId,
  signSubject,
  verifyId,
  personalSign,
  personalVerify,
  createIdentity,
  subject2Identity,
  identity2subject,
  getCredentials,
  getSubjectData,
} from './metamask';

const provider = new ethers.providers.Web3Provider(wallet, 'any');

export const onRpcRequest = async ({ origin, request }) => {
  // const privKey = await getPrivateKey();
  // console.log(`privKey is ${JSON.stringify(privKey)}`);
  // const ethWallet = new ethers.Wallet(privKey, provider);
  // console.dir(ethWallet);
  let subject = '';
  let identity = '';
  switch (request.method) {
    case 'address':
      return ethWallet.address;
    case 'signId':
      subject = request.params;
      if (
        await prompt({
          prompt: 'Assinar Dados',
          description: `Assinar os seguintes dados de ${origin}`,
          textAreaContent: JSON.stringify(subject, undefined, 2),
        })
      ) {
        return await signId(subject2Identity(subject));
      }
      return [];
    case 'signSubject':
      subject = request.params;
      if (
        await prompt({
          prompt: 'Assinar Dados',
          description: `Assinar os seguintes dados de ${origin}`,
          textAreaContent: JSON.stringify(subject, undefined, 2),
        })
      ) {
        return await signSubject(subject);
      }
      return {};
    case 'verifyId':
      identity = request.params;
      if (
        await prompt({
          prompt: 'Verificar Dados',
          description: `Verificar os seguintes dados de ${origin}`,
          textAreaContent: JSON.stringify(
            identity2subject(identity),
            undefined,
            2,
          ),
        })
      ) {
        return await verifyId(identity);
      }
      return [];
    case 'addState':
      return await addState(request.params);
    case 'getSubjectData':
      if (
        await prompt({
          prompt: 'Dados Requisitados',
          description: `Os seguintes dados estão sendo requisitados por ${origin}`,
          textAreaContent: JSON.stringify(request.params, undefined, 2),
        })
      ) {
        // todo: get only asked data
        return await getSubjectData(request.params);
      }
      return [];
    case 'setState':
      try {
        identity = request.params;
        const result = await updateState(identity);
        const msg = result
          ? `Dados salvos: ${JSON.stringify(await selectedAddress())}`
          : 'Falha ao salvar dados';
        await notify({ type: 'inApp', message: msg.substring(0, 49) });
        return result;
      } catch (err) {
        console.error(err);
        throw new Error(`Error: ${err.message}`);
      }
    case 'getState':
      return await getState();
    case 'getCredentials':
      return await getCredentials(
        request.params.contractAddress,
        request.params.userAddress,
      );
    case 'getEncryptionPublicKey':
      return `0x${Buffer.from(
        await getEncryptionPublicKey(),
        'base64',
      ).toString('hex')}`;
    default:
      // throw new Error('Method not found.');
      throw ethErrors.rpc.methodNotFound();
  }
};
