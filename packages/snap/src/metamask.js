/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { ethers } from 'ethers';
import {
  getBIP44AddressKeyDeriver,
  deriveBIP44AddressKey,
} from '@metamask/key-tree';
import { abi } from './IdentityNTT';

const provider = new ethers.providers.Web3Provider(wallet, 'any');

/*
IdentityType {
  version: number;
  subject: string;
  encrypted: boolean;
  data: string;
  user: address;
  issuer: address;
  issuerName: string;
  networkId: number;
  contract: address;
  signature: string;
  verify: boolean;
}
*/

export const prompt = async (parametros) => {
  return wallet
    .request({
      method: 'snap_confirm',
      params: [parametros],
    })
    .then((result) => {
      if (result === true) {
        return true;
      }
      return false;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
};

export const notify = async (parametros) => {
  return wallet
    .request({
      method: 'snap_notify',
      params: [parametros],
    })
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
};

export const clearState = async () => {
  return wallet
    .request({
      method: 'snap_manageState',
      params: ['clear'],
    })
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
};

export const updateState = async (identity) => {
  const state = { id: identity };
  return wallet
    .request({
      method: 'snap_manageState',
      params: ['update', state],
    })
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
};

export const getState = async () => {
  return wallet
    .request({
      method: 'snap_manageState',
      params: ['get'],
    })
    .then((state) => {
      if (
        state === null ||
        (typeof state === 'object' && state.id === undefined)
      ) {
        return [];
      } else if (state) {
        return state.id;
      }
      return [];
    })
    .catch((error) => {
      console.error(error);
      return [];
    });
};

export const addState = async (identity) => {
  const currentState = await getState();
  // todo: do not duplicate
  // todo: delete diferent subject data
  return await updateState([].concat(identity, currentState));
};

export const selectedAddress = async () => {
  const accounts = await wallet.request({
    method: 'eth_requestAccounts',
  });
  if (accounts) {
    return accounts.length > 0 ? accounts[0] : '';
  }
  return '';
};

export const getSubjectData = async (subject, networkId) => {
  const user = await selectedAddress();
  const identity = await getState();
  return await identity.filter((currentIdentity) => {
    return (
      currentIdentity.user.toLowerCase() === user.toLowerCase() &&
      subject.includes(currentIdentity.subject) &&
      (!networkId || currentIdentity.networkId === networkId)
    );
  });
};

export const stringToHex = function (value) {
  return `0x${Buffer.from(JSON.stringify(value), 'utf8').toString('hex')}`;
};

export const personalSign = async (msg, password = 'MinhaSenha') => {
  const hexMsg = stringToHex(msg);
  const from = await selectedAddress();
  return await wallet.request({
    method: 'personal_sign',
    params: [hexMsg, from, password],
  });
};

export const personalRecover = async (msg, sign) => {
  const hexMsg = stringToHex(msg);
  return await wallet.request({
    method: 'personal_ecRecover',
    params: [hexMsg, sign],
  });
};

export const personalVerify = async (msg, sign, from) => {
  const account = await personalRecover(msg, sign);
  return account === from;
};

export const netVersion = async () => {
  return await wallet.request({ method: 'net_version' });
};

// eslint-disable-next-line consistent-return
export const getPrivateKey = async () => {
  const etherNode = await wallet.request({
    method: 'snap_getBip44Entropy',
    params: {
      coinType: 60,
    },
  });
  if (etherNode) {
    // console.log(`etherNode is ${JSON.stringify(etherNode)}`);
    // const deriveEtherAddress = await getBIP44AddressKeyDeriver(etherNode);
    // return await deriveEtherAddress(0);
    const BIP44AddressKey = await deriveBIP44AddressKey(etherNode, {
      account: 0,
      change: 0,
      address_index: 0,
    });
    // console.log(`BIP44 AddressKey is ${JSON.stringify(BIP44AddressKey)}`);
    return BIP44AddressKey.privateKey;
  }
};

// eslint-disable-next-line consistent-return
export const getPubkicKey = async () => {
  const etherNode = await wallet.request({
    method: 'snap_getBip44Entropy',
    params: {
      coinType: 60,
    },
  });
  if (etherNode) {
    // console.log(`etherNode is ${JSON.stringify(etherNode)}`);
    // const deriveEtherAddress = await getBIP44AddressKeyDeriver(etherNode);
    // return await deriveEtherAddress(0);
    const BIP44AddressKey = await deriveBIP44AddressKey(etherNode, {
      account: 0,
      change: 0,
      address_index: 0,
    });
    // console.log(`BIP44 AddressKey is ${JSON.stringify(BIP44AddressKey)}`);
    return BIP44AddressKey.publicKey;
  }
};

export const getEncryptionPublicKey = async () => {
  const user = await selectedAddress();
  console.log(`User is ${JSON.stringify(user)}`);
  return wallet.request({
    method: 'eth_getEncryptionPublicKey',
    params: [user],
  });
};

export const signMessage = async (msg) => {
  const privKey = await getPrivateKey();
  // console.log(`privKey is ${JSON.stringify(privKey)}`);
  const ethWallet = new ethers.Wallet(privKey, provider);
  // console.dir(ethWallet);
  return await ethWallet.signMessage(msg);
};

export const signSubject = async (subject) => {
  const privKey = await getPrivateKey();
  const ethWallet = new ethers.Wallet(privKey, provider);
  return await Object.entries(subject).reduce(
    async (previousValue, [key, value]) => {
      const result = await previousValue;
      result[key] = await ethWallet.signMessage(value);
      return result;
    },
    {},
  );
};

export const subject2Identity = (subject, version = 1) => {
  return Object.entries(subject).reduce((previousValue, [key, value]) => {
    const identity = {};
    identity.version = version;
    identity.subject = key;
    identity.encrypted = false;
    identity.data = value;
    previousValue.push(identity);
    return previousValue;
  }, []);
};

export const createIdentity = async (request, version = 1) => {
  const user = await selectedAddress();
  const networkId = parseInt(await netVersion(), 10);
  if (request.attestation && request.attestation.length > 0) {
    return Object.entries(request.subject).reduce(
      (previousValue, [key, value]) => {
        request.attestation.forEach((currentValue) => {
          const identity = {};
          identity.version = version;
          identity.subject = key;
          identity.encrypted = false;
          identity.data = value;
          identity.user = user;

          if (currentValue.contract) {
            if (currentValue.networkId) {
              identity.networkId = currentValue.networkId;
            } else {
              identity.networkId = networkId;
            }
            identity.contract = currentValue.contract;
            if (currentValue.issuer) {
              identity.issuer = currentValue.issuer;
            }

            if (currentValue.issuerName) {
              identity.issuerName = currentValue.issuerName;
            }
          }

          previousValue.push(identity);
        });
        return previousValue;
      },
      [],
    );
  }
  return subject2Identity(request.subject, version);
};

export const identity2subject = (identity) => {
  return identity.reduce((previousValue, currentValue) => {
    const subject = {};
    // eslint-disable-next-line no-negated-condition
    if (!currentValue.encrypted) {
      subject[currentValue.subject] = currentValue.data;
    } else {
      // not yet implemented
    }
    return Object.assign({}, previousValue, subject);
  }, {});
};

export const signId = async (identity) => {
  const privKey = await getPrivateKey();
  const ethWallet = new ethers.Wallet(privKey, provider);
  return await Promise.all(
    identity.map(async (currentValue) => {
      const sign = {};
      // eslint-disable-next-line no-negated-condition
      if (!currentValue.encrypted) {
        sign.signature = await ethWallet.signMessage(currentValue.data);
        sign.issuer = await ethWallet.getAddress();
      } else {
        // not yet implemented
      }
      return Object.assign({}, currentValue, sign);
    }),
  );
};

export const verifyMessageAddress = async (msg, sign) => {
  return await ethers.utils.verifyMessage(msg, sign);
};

export const verifyMessage = async (msg, sign, address) => {
  return (await verifyMessageAddress(msg, sign)) === address;
};

export const verifyId = async (identity) => {
  return await Promise.all(
    identity.map(async (currentValue) => {
      const verification = {};
      // eslint-disable-next-line no-negated-condition
      if (!currentValue.encrypted) {
        verification.verify = await verifyMessage(
          currentValue.data,
          currentValue.signature,
          currentValue.issuer,
        );
      } else {
        // not yet implemented
      }
      console.log(`currentValue is ${JSON.stringify(currentValue)}`);
      return Object.assign({}, currentValue, verification);
    }),
  );
};

export const getCredentials = async (deployedContractAddress, owner) => {
  // console.log(`contract: ${deployedContractAddress}, owner: ${owner}`);
  const user = await selectedAddress();
  // console.log(`user: ${user}`);
  const nttContract = new ethers.Contract(
    deployedContractAddress,
    abi,
    provider.getSigner(user),
  );
  // console.log(`nome contract: ${await nttContract.name()}`);
  return await nttContract.getCredentials(owner, {
    from: user,
  });
};
