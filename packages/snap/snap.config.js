/* eslint-disable import/unambiguous */
module.exports = {
  cliOptions: {
    src: './src/index.js',
    port: 8080,
    transpilationMode: 'localAndDeps',
  },
};
