/* eslint-disable import/unambiguous */
module.exports = {
  extends: ['../../.eslintrc.js'],
  parserOptions: {
    sourceType: 'module',
  },
  ignorePatterns: ['!.eslintrc.js', 'dist/'],
};
